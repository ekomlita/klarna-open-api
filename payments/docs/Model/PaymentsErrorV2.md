# # PaymentsErrorV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorized_payment_method** | [**\KlarnaPaymentsApi\Model\PaymentsAuthorizedPaymentMethod**](PaymentsAuthorizedPaymentMethod.md) |  | [optional]
**correlation_id** | **string** |  | [optional]
**error_code** | **string** |  | [optional]
**error_messages** | **string[]** |  | [optional]
**fraud_status** | **string** |  | [optional]
**reason** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
