<?php
/**
 * PaymentsOrderLineTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaPaymentsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Payments API V1
 *
 * API spec
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace KlarnaPaymentsApi\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * PaymentsOrderLineTest Class Doc Comment
 *
 * @category    Class
 * @description PaymentsOrderLine
 * @package     KlarnaPaymentsApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class PaymentsOrderLineTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "PaymentsOrderLine"
     */
    public function testPaymentsOrderLine()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "image_url"
     */
    public function testPropertyImageUrl()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "merchant_data"
     */
    public function testPropertyMerchantData()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "product_identifiers"
     */
    public function testPropertyProductIdentifiers()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "product_url"
     */
    public function testPropertyProductUrl()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "quantity"
     */
    public function testPropertyQuantity()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "quantity_unit"
     */
    public function testPropertyQuantityUnit()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "reference"
     */
    public function testPropertyReference()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "tax_rate"
     */
    public function testPropertyTaxRate()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "total_amount"
     */
    public function testPropertyTotalAmount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "total_discount_amount"
     */
    public function testPropertyTotalDiscountAmount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "total_tax_amount"
     */
    public function testPropertyTotalTaxAmount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "unit_price"
     */
    public function testPropertyUnitPrice()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
