<?php
/**
 * PaymentsOptionsTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaPaymentsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Payments API V1
 *
 * API spec
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace KlarnaPaymentsApi\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * PaymentsOptionsTest Class Doc Comment
 *
 * @category    Class
 * @description PaymentsOptions
 * @package     KlarnaPaymentsApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class PaymentsOptionsTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "PaymentsOptions"
     */
    public function testPaymentsOptions()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "color_border"
     */
    public function testPropertyColorBorder()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "color_border_selected"
     */
    public function testPropertyColorBorderSelected()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "color_details"
     */
    public function testPropertyColorDetails()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "color_text"
     */
    public function testPropertyColorText()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "radius_border"
     */
    public function testPropertyRadiusBorder()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
