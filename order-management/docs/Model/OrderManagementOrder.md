# # OrderManagementOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **string** | The unique order ID. Cannot be longer than 255 characters. | [optional]
**status** | **string** | The order status. | [optional]
**fraud_status** | **string** | Fraud status for the order. Either ACCEPTED, PENDING or REJECTED. | [optional]
**order_amount** | **int** | The order amount in minor units. That is the smallest currency unit available such as cent or penny. | [optional]
**original_order_amount** | **int** | The original order amount. In minor units. | [optional]
**captured_amount** | **int** | The total amount of all captures. In minor units. | [optional]
**refunded_amount** | **int** | The total amount of refunded for this order. In minor units. | [optional]
**remaining_authorized_amount** | **int** | The remaining authorized amount for this order. To increase the &#x60;remaining_authorized_amount&#x60; the &#x60;order_amount&#x60; needs to be increased. | [optional]
**purchase_currency** | **string** | The currency for this order. Specified in ISO 4217 format. | [optional]
**locale** | **string** | The customers locale. Specified according to RFC 1766. | [optional]
**order_lines** | [**\KlarnaOrderManagementApi\Model\OrderManagementOrderLine[]**](OrderManagementOrderLine.md) | An array of order_line objects. Each line represents one item in the cart. | [optional]
**merchant_reference1** | **string** | The order number that the merchant should assign to the order. This is how a customer would reference the purchase they made. If supplied, it is labeled as the Order Number within post purchase communications as well as the Klarna App. | [optional]
**merchant_reference2** | **string** | Can be used to store your internal reference to the order. This is generally an internal reference number that merchants use as alternate identifier that matches their internal ERP or Order Management system. | [optional]
**klarna_reference** | **string** | A Klarna generated reference that is shorter than the Klarna Order Id and is used as a customer friendly reference. It is most often used as a reference when Klarna is communicating with the customer with regard to payment statuses. | [optional]
**customer** | [**\KlarnaOrderManagementApi\Model\OrderManagementCustomer**](OrderManagementCustomer.md) |  | [optional]
**billing_address** | [**\KlarnaOrderManagementApi\Model\OrderManagementAddress**](OrderManagementAddress.md) |  | [optional]
**shipping_address** | [**\KlarnaOrderManagementApi\Model\OrderManagementAddress**](OrderManagementAddress.md) |  | [optional]
**created_at** | **\DateTime** | The time for the purchase. Formatted according to ISO 8601. | [optional]
**purchase_country** | **string** | The purchase country. Formatted according to ISO 3166-1 alpha-2. | [optional]
**expires_at** | **\DateTime** | Order expiration time. The order can only be captured until this time. Formatted according to ISO 8601. | [optional]
**captures** | [**\KlarnaOrderManagementApi\Model\OrderManagementCapture[]**](OrderManagementCapture.md) | List of captures for this order. | [optional]
**refunds** | [**\KlarnaOrderManagementApi\Model\OrderManagementRefund[]**](OrderManagementRefund.md) | List of refunds for this order. | [optional]
**merchant_data** | **string** | Text field for storing data about the order. Set at order creation. | [optional]
**initial_payment_method** | [**\KlarnaOrderManagementApi\Model\OrderManagementInitialPaymentMethodDto**](OrderManagementInitialPaymentMethodDto.md) |  | [optional]
**selected_shipping_option** | [**\KlarnaOrderManagementApi\Model\OrderManagementSelectedShippingOptionDto**](OrderManagementSelectedShippingOptionDto.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
