# # OrderManagementUpdateShippingInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_info** | [**\KlarnaOrderManagementApi\Model\OrderManagementShippingInfo[]**](OrderManagementShippingInfo.md) | New shipping info. Maximum: 500 items. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
