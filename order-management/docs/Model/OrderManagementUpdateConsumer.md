# # OrderManagementUpdateConsumer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_address** | [**\KlarnaOrderManagementApi\Model\OrderManagementAddress**](OrderManagementAddress.md) |  | [optional]
**billing_address** | [**\KlarnaOrderManagementApi\Model\OrderManagementAddress**](OrderManagementAddress.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
