# # OrderManagementLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The location id | [optional]
**name** | **string** | The display name of the location | [optional]
**price** | **int** | The price for this location | [optional]
**address** | [**\KlarnaOrderManagementApi\Model\OrderManagementAddress**](OrderManagementAddress.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
