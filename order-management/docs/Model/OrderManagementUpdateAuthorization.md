# # OrderManagementUpdateAuthorization

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_amount** | **int** | The new total order amount. Minor units. |
**description** | **string** | Description of the change. | [optional]
**order_lines** | [**\KlarnaOrderManagementApi\Model\OrderManagementOrderLine[]**](OrderManagementOrderLine.md) | New set of order lines for the order. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
