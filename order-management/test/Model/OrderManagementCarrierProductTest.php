<?php
/**
 * OrderManagementCarrierProductTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaOrderManagementApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Order Managment API
 *
 * API to handle order lifecycle
 *
 * The version of the OpenAPI document: 1.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace KlarnaOrderManagementApi\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * OrderManagementCarrierProductTest Class Doc Comment
 *
 * @category    Class
 * @description OrderManagementCarrierProduct
 * @package     KlarnaOrderManagementApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class OrderManagementCarrierProductTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "OrderManagementCarrierProduct"
     */
    public function testOrderManagementCarrierProduct()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "identifier"
     */
    public function testPropertyIdentifier()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
