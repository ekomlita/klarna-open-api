<?php
/**
 * OrderCancellationApiTest
 * PHP version 7.3
 *
 * @category Class
 * @package  KlarnaMerchantCardServiceApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Merchant Card Service API
 *
 * The Merchant Card Service (MCS) API is used to settle orders with virtual credit cards.
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace KlarnaMerchantCardServiceApi\Test\Api;

use \KlarnaMerchantCardServiceApi\Configuration;
use \KlarnaMerchantCardServiceApi\ApiException;
use \KlarnaMerchantCardServiceApi\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * OrderCancellationApiTest Class Doc Comment
 *
 * @category Class
 * @package  KlarnaMerchantCardServiceApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class OrderCancellationApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for getcancelorder
     *
     * Get status of the order cancellation request.
     *
     */
    public function testGetcancelorder()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postcancelorder
     *
     * Request cancellation for an order.
     *
     */
    public function testPostcancelorder()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
