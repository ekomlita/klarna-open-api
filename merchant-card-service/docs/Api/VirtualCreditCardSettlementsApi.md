# KlarnaMerchantCardServiceApi\VirtualCreditCardSettlementsApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**readSettlement()**](VirtualCreditCardSettlementsApi.md#readSettlement) | **GET** /merchantcard/v3/settlements/{settlement_id} | Retrieve an existing settlement
[**readSettlementByOrderId()**](VirtualCreditCardSettlementsApi.md#readSettlementByOrderId) | **GET** /merchantcard/v3/settlements/order/{order_id} | Retrieve a settled order&#39;s settlement
[**settlePromise()**](VirtualCreditCardSettlementsApi.md#settlePromise) | **POST** /merchantcard/v3/settlements | Create a new settlement


## `readSettlement()`

```php
readSettlement($settlement_id, $key_id): \KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementResponse
```

Retrieve an existing settlement

To read the settlement resource provide the settlement identifier.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaMerchantCardServiceApi\Api\VirtualCreditCardSettlementsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$settlement_id = b0ec0bbd-534c-4b1c-b28a-628bf33c3324; // string | Unique settlement identifier.
$key_id = 'key_id_example'; // string | Unique identifier for the public key used for encryption of the card data.

try {
    $result = $apiInstance->readSettlement($settlement_id, $key_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VirtualCreditCardSettlementsApi->readSettlement: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **settlement_id** | **string**| Unique settlement identifier. |
 **key_id** | **string**| Unique identifier for the public key used for encryption of the card data. |

### Return type

[**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementResponse**](../Model/MerchantCardServiceSettlementResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readSettlementByOrderId()`

```php
readSettlementByOrderId($order_id, $key_id): \KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementResponse
```

Retrieve a settled order's settlement

To read the order's settlement resource provide the order identifier.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaMerchantCardServiceApi\Api\VirtualCreditCardSettlementsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_id = f3392f8b-6116-4073-ab96-e330819e2c07; // string | Unique identifier for the order associated to the settlement.
$key_id = 'key_id_example'; // string | Unique identifier for the public key used for encryption of the card data.

try {
    $result = $apiInstance->readSettlementByOrderId($order_id, $key_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VirtualCreditCardSettlementsApi->readSettlementByOrderId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**| Unique identifier for the order associated to the settlement. |
 **key_id** | **string**| Unique identifier for the public key used for encryption of the card data. |

### Return type

[**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementResponse**](../Model/MerchantCardServiceSettlementResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `settlePromise()`

```php
settlePromise($merchant_card_service_settlement_request): \KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementResponse
```

Create a new settlement

To create a settlement resource provide a completed order identifier and (optionally) a promise identifier.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaMerchantCardServiceApi\Api\VirtualCreditCardSettlementsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$merchant_card_service_settlement_request = new \KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementRequest(); // \KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementRequest

try {
    $result = $apiInstance->settlePromise($merchant_card_service_settlement_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VirtualCreditCardSettlementsApi->settlePromise: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_card_service_settlement_request** | [**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementRequest**](../Model/MerchantCardServiceSettlementRequest.md)|  | [optional]

### Return type

[**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceSettlementResponse**](../Model/MerchantCardServiceSettlementResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
