# KlarnaMerchantCardServiceApi\OrderCancellationApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getcancelorder()**](OrderCancellationApi.md#getcancelorder) | **GET** /merchantcard/v3/orders/{order_id}/cancel-request | Get status of the order cancellation request
[**postcancelorder()**](OrderCancellationApi.md#postcancelorder) | **POST** /merchantcard/v3/orders/{order_id}/cancel-request | Request cancellation for an order


## `getcancelorder()`

```php
getcancelorder($order_id): \KlarnaMerchantCardServiceApi\Model\MerchantCardServiceCancelOrderRequestStatus
```

Get status of the order cancellation request

Get the status of an order cancellation request. The order must have an associated Virtual Credit Card Settlement.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaMerchantCardServiceApi\Api\OrderCancellationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_id = f3392f8b-6116-4073-ab96-e330819e2c07; // string | Order id for which to view its cancellation status. This order must have an associated Virtual Credit Card Settlement.

try {
    $result = $apiInstance->getcancelorder($order_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderCancellationApi->getcancelorder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**| Order id for which to view its cancellation status. This order must have an associated Virtual Credit Card Settlement. |

### Return type

[**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceCancelOrderRequestStatus**](../Model/MerchantCardServiceCancelOrderRequestStatus.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postcancelorder()`

```php
postcancelorder($order_id): \KlarnaMerchantCardServiceApi\Model\MerchantCardServiceCancelOrderRequestStatusCancelled
```

Request cancellation for an order

Request cancellation for an order. If the order is already cancelled, a `200` status is returned.  Otherwise, the order will be queued for cancellation with a `202` status. Actual order cancellation will happen asynchronously at a later time. You can call the corresponding GET endpoint to view the status of the request.  This cancellation endpoint is limited to the scope of the Virtual Credit Cards product. Therefore the order provided must have an associated Virtual Card Settlement, otherwise the call will fail.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaMerchantCardServiceApi\Api\OrderCancellationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_id = f3392f8b-6116-4073-ab96-e330819e2c07; // string | Order id you wish to cancel. This order must have an associated Virtual Credit Card Settlement.

try {
    $result = $apiInstance->postcancelorder($order_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrderCancellationApi->postcancelorder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **string**| Order id you wish to cancel. This order must have an associated Virtual Credit Card Settlement. |

### Return type

[**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceCancelOrderRequestStatusCancelled**](../Model/MerchantCardServiceCancelOrderRequestStatusCancelled.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
