# KlarnaMerchantCardServiceApi\VirtualCreditCardPromisesApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPromise()**](VirtualCreditCardPromisesApi.md#createPromise) | **POST** /merchantcard/v3/promises | Create a new promise
[**readPromise()**](VirtualCreditCardPromisesApi.md#readPromise) | **GET** /merchantcard/v3/promises/{promise_id} | Retrieve an existing promise


## `createPromise()`

```php
createPromise($merchant_card_service_promise_request): \KlarnaMerchantCardServiceApi\Model\MerchantCardServicePromiseCreatedResponse
```

Create a new promise

To create promise provide a purchase currency and the cards to be created. The old promise is automatically invalidated if a new promise is created for an order

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaMerchantCardServiceApi\Api\VirtualCreditCardPromisesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$merchant_card_service_promise_request = new \KlarnaMerchantCardServiceApi\Model\MerchantCardServicePromiseRequest(); // \KlarnaMerchantCardServiceApi\Model\MerchantCardServicePromiseRequest

try {
    $result = $apiInstance->createPromise($merchant_card_service_promise_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VirtualCreditCardPromisesApi->createPromise: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_card_service_promise_request** | [**\KlarnaMerchantCardServiceApi\Model\MerchantCardServicePromiseRequest**](../Model/MerchantCardServicePromiseRequest.md)|  | [optional]

### Return type

[**\KlarnaMerchantCardServiceApi\Model\MerchantCardServicePromiseCreatedResponse**](../Model/MerchantCardServicePromiseCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readPromise()`

```php
readPromise($promise_id): \KlarnaMerchantCardServiceApi\Model\MerchantCardServicePromiseResponse
```

Retrieve an existing promise

To read the promise resource simply provide a promise identifier.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaMerchantCardServiceApi\Api\VirtualCreditCardPromisesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$promise_id = ee4a8e3a-9dfd-49e0-9ac8-ea2b6c76408c; // string

try {
    $result = $apiInstance->readPromise($promise_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VirtualCreditCardPromisesApi->readPromise: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **promise_id** | **string**|  |

### Return type

[**\KlarnaMerchantCardServiceApi\Model\MerchantCardServicePromiseResponse**](../Model/MerchantCardServicePromiseResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
