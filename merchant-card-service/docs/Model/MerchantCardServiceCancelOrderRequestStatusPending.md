# # MerchantCardServiceCancelOrderRequestStatusPending

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | The status of the order cancellation request. |
**check_after** | **\DateTime** | Timestamp for when we expect a decision to be made on order cancellation. (ISO 8601) |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
