# # MerchantCardServiceSettlementRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promise_id** | **string** | Unique identifier for the promise associated to the settlement. | [optional]
**order_id** | **string** | Unique identifier for the order associated to the settlement. |
**key_id** | **string** | Unique identifier for the public key to be used for encryption of the card data. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
