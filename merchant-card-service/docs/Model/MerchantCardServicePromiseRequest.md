# # MerchantCardServicePromiseRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **string** | A valid order id |
**cards** | [**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceCardSpecification[]**](MerchantCardServiceCardSpecification.md) | The cards you would like to issue (max 1000) |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
