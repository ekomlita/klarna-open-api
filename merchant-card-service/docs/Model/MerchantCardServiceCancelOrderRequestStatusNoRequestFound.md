# # MerchantCardServiceCancelOrderRequestStatusNoRequestFound

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | The status of the order cancellation request. |
**reason_code** | **string** | Reason code for the bad request. |
**reason_message** | **string** | Human-readable message for the bad request. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
