# # MerchantCardServiceCard

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference** | **string** | Identifier to reference order line. | [optional]
**card_id** | **string** | Unique card identifier. | [optional]
**amount** | **int** | The total amount available on the card. In minor units. The number of decimals are controlled by the currency. | [optional]
**currency** | **string** | The ISO 4217 code states which currency it is and how many decimals the amount has. | [optional]
**pci_data** | **string** | Encrypted, PCI compliant card data. | [optional]
**iv** | **string** | Initialization vector for symmetric decryption with the AES key. | [optional]
**aes_key** | **string** | The symmetric key complying the Advanced Encryption Standard. | [optional]
**brand** | **string** | The brand of the card. | [optional]
**holder** | **string** | Card holder name on the card. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
