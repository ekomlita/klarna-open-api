# # MerchantCardServiceCardSpecification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **int** | The total purchase amount on a card |
**currency** | **string** | The amount currency |
**fund_amount** | **int** | The funded amount that will be on a card | [optional]
**reference** | **string** | Your reference on the card |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
