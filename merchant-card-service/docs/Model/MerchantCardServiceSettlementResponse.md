# # MerchantCardServiceSettlementResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settlement_id** | **string** | Unique settlement identifier. | [optional]
**promise_id** | **string** | Unique identifier for the promise associated to the settlement. | [optional]
**order_id** | **string** | Unique identifier for the order associated to the settlement. | [optional]
**cards** | [**\KlarnaMerchantCardServiceApi\Model\MerchantCardServiceCard[]**](MerchantCardServiceCard.md) | An array of Card objects. | [optional]
**created_at** | **string** | Settlement creation datetime (ISO 8601). | [optional]
**expires_at** | **string** | Settlement expiration datetime (ISO 8601). | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
