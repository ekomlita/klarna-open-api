# # MerchantCardServiceCancelOrderRequestStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | The status of the order cancellation request. |
**check_after** | **\DateTime** | Timestamp for when we expect a decision to be made on order cancellation. (ISO 8601)  This field is only populated if status is &#x60;PENDING&#x60;. | [optional]
**reason_code** | **string** | Reason code for why the order cancellation was rejected.  This field is only populated if status is &#x60;REJECTED&#x60;. | [optional]
**reason_message** | **string** | Human-readable message for why the order cancellation was rejected.  This field is only populated if status is &#x60;REJECTED&#x60;. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
