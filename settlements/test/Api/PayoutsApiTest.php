<?php
/**
 * PayoutsApiTest
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Settlements API
 *
 * This API gives you access to your payouts and transactions.     Resources are split into two broad types:     * Collections, including pagination information:      collections are queryable, typically by the attributes of the sub-resource      as well as pagination.    * Entity resources containing a single entity.
 *
 * The version of the OpenAPI document: 1.0.0-rc2
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace KlarnaSettlementsApi\Test\Api;

use \KlarnaSettlementsApi\Configuration;
use \KlarnaSettlementsApi\ApiException;
use \KlarnaSettlementsApi\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * PayoutsApiTest Class Doc Comment
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PayoutsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for getPayout
     *
     * Get payout.
     *
     */
    public function testGetPayout()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getPayoutSummary
     *
     * Get summary of payouts.
     *
     */
    public function testGetPayoutSummary()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getPayouts
     *
     * Get all payouts.
     *
     */
    public function testGetPayouts()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
