# KlarnaSettlementsApi\TransactionsApi

All URIs are relative to https://api.klarna.com/settlements/v1.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTransactions()**](TransactionsApi.md#getTransactions) | **GET** /transactions | Get transactions


## `getTransactions()`

```php
getTransactions($payment_reference, $order_id, $size, $offset): \KlarnaSettlementsApi\Model\SettlementsTransactionCollection
```

Get transactions

Returns a collection of transactions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\TransactionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_reference = 'payment_reference_example'; // string | The reference id of the payout
$order_id = 'order_id_example'; // string | The Klarna assigned order id reference
$size = 20; // int | How many elements to include in the result. If no value for size is provided, a default of 20 will be used.
$offset = 0; // int | The current offset. Describes \"where\" in a collection the current starts.

try {
    $result = $apiInstance->getTransactions($payment_reference, $order_id, $size, $offset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionsApi->getTransactions: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_reference** | **string**| The reference id of the payout | [optional]
 **order_id** | **string**| The Klarna assigned order id reference | [optional]
 **size** | **int**| How many elements to include in the result. If no value for size is provided, a default of 20 will be used. | [optional] [default to 20]
 **offset** | **int**| The current offset. Describes \&quot;where\&quot; in a collection the current starts. | [optional] [default to 0]

### Return type

[**\KlarnaSettlementsApi\Model\SettlementsTransactionCollection**](../Model/SettlementsTransactionCollection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
