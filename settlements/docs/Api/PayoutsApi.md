# KlarnaSettlementsApi\PayoutsApi

All URIs are relative to https://api.klarna.com/settlements/v1.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPayout()**](PayoutsApi.md#getPayout) | **GET** /payouts/{payment_reference} | Get payout
[**getPayoutSummary()**](PayoutsApi.md#getPayoutSummary) | **GET** /payouts/summary | Get summary of payouts
[**getPayouts()**](PayoutsApi.md#getPayouts) | **GET** /payouts | Get all payouts


## `getPayout()`

```php
getPayout($payment_reference): \KlarnaSettlementsApi\Model\SettlementsPayout
```

Get payout

Returns a specific payout based on a given payment reference.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\PayoutsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_reference = 'payment_reference_example'; // string | The reference id of the payout. Normally this reference can be found on your payment slip statement of your bank.

try {
    $result = $apiInstance->getPayout($payment_reference);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayoutsApi->getPayout: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_reference** | **string**| The reference id of the payout. Normally this reference can be found on your payment slip statement of your bank. |

### Return type

[**\KlarnaSettlementsApi\Model\SettlementsPayout**](../Model/SettlementsPayout.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPayoutSummary()`

```php
getPayoutSummary($start_date, $end_date, $currency_code): \KlarnaSettlementsApi\Model\SettlementsPayoutSummary[]
```

Get summary of payouts

Returns a summary of payouts for each currency code in a date range.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\PayoutsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -> 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z.
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full datetime like 2020-01-23T23:59:59Z.
$currency_code = 'currency_code_example'; // string | An optional currency code to filter the result for different currencies. If not provided the result returned in the response might include multiple results grouped by the currency. The currency should be provided by an ISO 4217 Currency Code like USD, EUR, AUD or GBP.

try {
    $result = $apiInstance->getPayoutSummary($start_date, $end_date, $currency_code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayoutsApi->getPayoutSummary: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -&gt; 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z. |
 **end_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full datetime like 2020-01-23T23:59:59Z. |
 **currency_code** | **string**| An optional currency code to filter the result for different currencies. If not provided the result returned in the response might include multiple results grouped by the currency. The currency should be provided by an ISO 4217 Currency Code like USD, EUR, AUD or GBP. | [optional]

### Return type

[**\KlarnaSettlementsApi\Model\SettlementsPayoutSummary[]**](../Model/SettlementsPayoutSummary.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPayouts()`

```php
getPayouts($start_date, $end_date, $currency_code, $size, $offset): \KlarnaSettlementsApi\Model\SettlementsPayoutCollection
```

Get all payouts

Returns a collection of payouts.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaSettlementsApi\Api\PayoutsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$start_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -> 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z.
$end_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full date time like 2020-01-23T23:59:59Z.
$currency_code = 'currency_code_example'; // string | An optional currency code to filter the result for different currencies. If not provided the result returned in the response might include multiple results grouped by the currency. The currency should be provided by an ISO 4217 Currency Code like USD, EUR, AUD or GBP.
$size = 20; // int | How many elements to include in the result. If no value for size is provided, a default of 20 will be used.
$offset = 0; // int | The current offset. Describes \"where\" in a collection the current starts.

try {
    $result = $apiInstance->getPayouts($start_date, $end_date, $currency_code, $size, $offset);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PayoutsApi->getPayouts: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. For example 2020-01-23 -&gt; 2020-01-23T00:00:00Z. For this reason we recommend too use the full datetime like 2020-01-23T00:00:00Z. | [optional]
 **end_date** | **\DateTime**| ISO 8601 date time format. This is a filter for the payout date. If no time is given then it defaults to the start of the day, ie 00:00:00. This might lead to unwanted side effects like when the start date and end date might be the same because it would request payouts between 2020-01-23T00:00:00Z and 2020-01-23T00:00:00Z. Instead we advise to use a full date time like 2020-01-23T23:59:59Z. | [optional]
 **currency_code** | **string**| An optional currency code to filter the result for different currencies. If not provided the result returned in the response might include multiple results grouped by the currency. The currency should be provided by an ISO 4217 Currency Code like USD, EUR, AUD or GBP. | [optional]
 **size** | **int**| How many elements to include in the result. If no value for size is provided, a default of 20 will be used. | [optional] [default to 20]
 **offset** | **int**| The current offset. Describes \&quot;where\&quot; in a collection the current starts. | [optional] [default to 0]

### Return type

[**\KlarnaSettlementsApi\Model\SettlementsPayoutCollection**](../Model/SettlementsPayoutCollection.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
