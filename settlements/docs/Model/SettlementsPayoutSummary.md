# # SettlementsPayoutSummary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary_total_fee_correction_amount** | **int** | The total amount of fee correction, in minor units |
**summary_payout_date_start** | **\DateTime** | ISO 8601 formatted date-time string |
**summary_total_release_amount** | **int** | The total amount of money released from holdback by Klarna, in minor units |
**summary_settlement_currency** | **string** | ISO 4217 Currency Code. Like USD, EUR, AUD or GBP. |
**summary_payout_date_end** | **\DateTime** | ISO 8601 formatted date-time string |
**summary_total_tax_amount** | **int** | The total amount of tax, in minor units |
**summary_total_settlement_amount** | **int** | The total amount of the settlement in question, in minor units |
**summary_total_holdback_amount** | **int** | The total amount of money withheld by Klarna, in minor units |
**summary_total_reversal_amount** | **int** | The total amount of reversals, in minor units |
**summary_total_return_amount** | **int** | The total amount of returns, in minor units |
**summary_total_fee_amount** | **int** | The total amount of fees, in minor units |
**summary_total_commission_amount** | **int** | The total amount of commissions, in minor units |
**summary_total_sale_amount** | **int** | The total amount of sales, in minor units |
**summary_total_repay_amount** | **int** | The total amount of money that has been repaid by the merchant from the debt to Klarna, in minor units |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
