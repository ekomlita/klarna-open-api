# # SettlementsTotals

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commission_amount** | **int** | The total amount of commissions, in minor units |
**repay_amount** | **int** | The total amount of money that has been repaid by the merchant from the debt to Klarna, in minor units |
**sale_amount** | **int** | The total amount of sales, in minor units |
**holdback_amount** | **int** | The total amount of money withheld by Klarna, in minor units |
**tax_amount** | **int** | The total amount of tax, in minor units | [optional]
**settlement_amount** | **int** | The total amount of the settlement in question, in minor units |
**fee_correction_amount** | **int** | The total amount of fee correction, in minor units | [optional]
**reversal_amount** | **int** | The total amount of reversals, in minor units |
**release_amount** | **int** | The total amount of money released from holdback by Klarna, in minor units |
**return_amount** | **int** | The total amount of returns, in minor units |
**fee_amount** | **int** | The total amount of fees, in minor units |
**charge_amount** | **int** | The total amount of charges, in minor units. The additional field detailed_type contains the purpose of the charge | [optional]
**credit_amount** | **int** | The total amount of credits, in minor units. The additional field detailed_type contains the purpose of the credit | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
