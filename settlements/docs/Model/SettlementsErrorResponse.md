# # SettlementsErrorResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_code** | **string** | ERROR_CODE |
**error_messages** | **string[]** | Array of error messages |
**correlation_id** | **string** | Unique id for this request used for troubleshooting. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
