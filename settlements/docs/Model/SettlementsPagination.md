# # SettlementsPagination

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | The amount of elements in the current result |
**total** | **int** | The total amount of elements that are available | [optional]
**next** | **string** | The URI to the next \&quot;page\&quot; of results. | [optional]
**prev** | **string** | The URI to the previous \&quot;page\&quot; of results. | [optional]
**offset** | **int** | The current offset. Describes \&quot;where\&quot; in a collection the current starts. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
