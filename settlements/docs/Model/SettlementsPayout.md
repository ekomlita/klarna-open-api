# # SettlementsPayout

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totals** | [**\KlarnaSettlementsApi\Model\SettlementsTotals**](SettlementsTotals.md) |  |
**payment_reference** | **string** | The reference id of the payout |
**payout_date** | **\DateTime** | ISO 8601 formatted date-time string |
**currency_code** | **string** | ISO 4217 Currency Code. Like USD, EUR, AUD or GBP. |
**currency_code_of_registration_country** | **string** | ISO 4217 Currency Code of the country you are registered in. | [optional]
**merchant_settlement_type** | **string** | Whether the amounts are net or gross |
**merchant_id** | **string** | The merchant id |
**transactions** | **string** | Link to the transactions that are part of this payout | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
