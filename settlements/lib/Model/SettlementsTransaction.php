<?php
/**
 * SettlementsTransaction
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Settlements API
 *
 * The Settlements API helps you with the reconciliation of payments, made by Klarna to your bank account. Every payment has a unique payment_reference that can be found in the settlement reports and on your bank statement.  Besides responses in JSON-format, we provide endpoints to download transactional CSV reports, as well as PDF Summary, reports. Both reports can be downloaded for a specific payout as well as aggregated for a time range.  ![](https://images.prismic.io/docsportal/b20b8140-ea4e-4b0a-a812-dfb459aa332d_FIRE-settlements_api_overview.png?auto=compress,format)  For the day-to-day reconciliation of payments we recommend the following:  - By performing the requests “get all payouts” you will get the totals of the payouts and the corresponding payment references as a response. The payment references are needed for the request “get transactions”.  - Perform the request \"get transactions\". The response contains all related transactions of a payout.  - If you prefer to use CSV-files for your reconciliation, you can query “Get CSV payout report” for a specific payment_reference.  Every transaction is related to a unique order_ID. All related transactions in the life-span of an order are associated with this ID. eg. fees or refunds. It is, therefore, the recommended identifier for reconciling the report lines with your system.  Resources are split into two broad types:  - Collections, including pagination information: collections are queryable, typically by the attributes of the sub-resource as well as pagination.  - Entity resources containing a single entity.
 *
 * The version of the OpenAPI document: 1.0.0-rc2
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace KlarnaSettlementsApi\Model;

use \ArrayAccess;
use \KlarnaSettlementsApi\ObjectSerializer;

/**
 * SettlementsTransaction Class Doc Comment
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class SettlementsTransaction implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'Transaction';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'amount' => 'int',
        'capture_id' => 'string',
        'merchant_reference1' => 'string',
        'sale_date' => '\DateTime',
        'type' => 'string',
        'capture_date' => '\DateTime',
        'payment_reference' => 'string',
        'order_id' => 'string',
        'payout' => 'string',
        'refund_id' => 'string',
        'short_order_id' => 'string',
        'merchant_reference2' => 'string',
        'currency_code' => 'string',
        'purchase_country' => 'string',
        'vat_rate' => 'int',
        'vat_amount' => 'int',
        'shipping_country' => 'string',
        'initial_payment_method_type' => 'string',
        'initial_number_of_installments' => 'int',
        'initial_payment_method_monthly_downpayments' => 'int',
        'merchant_capture_reference' => 'string',
        'merchant_refund_reference' => 'string',
        'detailed_type' => 'string',
        'tax_in_currency_of_registration_country' => 'int',
        'currency_code_of_registration_country' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'amount' => 'int64',
        'capture_id' => null,
        'merchant_reference1' => null,
        'sale_date' => 'date-time',
        'type' => null,
        'capture_date' => 'date-time',
        'payment_reference' => null,
        'order_id' => 'uuid',
        'payout' => null,
        'refund_id' => null,
        'short_order_id' => null,
        'merchant_reference2' => null,
        'currency_code' => null,
        'purchase_country' => null,
        'vat_rate' => null,
        'vat_amount' => null,
        'shipping_country' => null,
        'initial_payment_method_type' => null,
        'initial_number_of_installments' => null,
        'initial_payment_method_monthly_downpayments' => null,
        'merchant_capture_reference' => null,
        'merchant_refund_reference' => null,
        'detailed_type' => null,
        'tax_in_currency_of_registration_country' => null,
        'currency_code_of_registration_country' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'amount' => 'amount',
        'capture_id' => 'capture_id',
        'merchant_reference1' => 'merchant_reference1',
        'sale_date' => 'sale_date',
        'type' => 'type',
        'capture_date' => 'capture_date',
        'payment_reference' => 'payment_reference',
        'order_id' => 'order_id',
        'payout' => 'payout',
        'refund_id' => 'refund_id',
        'short_order_id' => 'short_order_id',
        'merchant_reference2' => 'merchant_reference2',
        'currency_code' => 'currency_code',
        'purchase_country' => 'purchase_country',
        'vat_rate' => 'vat_rate',
        'vat_amount' => 'vat_amount',
        'shipping_country' => 'shipping_country',
        'initial_payment_method_type' => 'initial_payment_method_type',
        'initial_number_of_installments' => 'initial_number_of_installments',
        'initial_payment_method_monthly_downpayments' => 'initial_payment_method_monthly_downpayments',
        'merchant_capture_reference' => 'merchant_capture_reference',
        'merchant_refund_reference' => 'merchant_refund_reference',
        'detailed_type' => 'detailed_type',
        'tax_in_currency_of_registration_country' => 'tax_in_currency_of_registration_country',
        'currency_code_of_registration_country' => 'currency_code_of_registration_country'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'amount' => 'setAmount',
        'capture_id' => 'setCaptureId',
        'merchant_reference1' => 'setMerchantReference1',
        'sale_date' => 'setSaleDate',
        'type' => 'setType',
        'capture_date' => 'setCaptureDate',
        'payment_reference' => 'setPaymentReference',
        'order_id' => 'setOrderId',
        'payout' => 'setPayout',
        'refund_id' => 'setRefundId',
        'short_order_id' => 'setShortOrderId',
        'merchant_reference2' => 'setMerchantReference2',
        'currency_code' => 'setCurrencyCode',
        'purchase_country' => 'setPurchaseCountry',
        'vat_rate' => 'setVatRate',
        'vat_amount' => 'setVatAmount',
        'shipping_country' => 'setShippingCountry',
        'initial_payment_method_type' => 'setInitialPaymentMethodType',
        'initial_number_of_installments' => 'setInitialNumberOfInstallments',
        'initial_payment_method_monthly_downpayments' => 'setInitialPaymentMethodMonthlyDownpayments',
        'merchant_capture_reference' => 'setMerchantCaptureReference',
        'merchant_refund_reference' => 'setMerchantRefundReference',
        'detailed_type' => 'setDetailedType',
        'tax_in_currency_of_registration_country' => 'setTaxInCurrencyOfRegistrationCountry',
        'currency_code_of_registration_country' => 'setCurrencyCodeOfRegistrationCountry'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'amount' => 'getAmount',
        'capture_id' => 'getCaptureId',
        'merchant_reference1' => 'getMerchantReference1',
        'sale_date' => 'getSaleDate',
        'type' => 'getType',
        'capture_date' => 'getCaptureDate',
        'payment_reference' => 'getPaymentReference',
        'order_id' => 'getOrderId',
        'payout' => 'getPayout',
        'refund_id' => 'getRefundId',
        'short_order_id' => 'getShortOrderId',
        'merchant_reference2' => 'getMerchantReference2',
        'currency_code' => 'getCurrencyCode',
        'purchase_country' => 'getPurchaseCountry',
        'vat_rate' => 'getVatRate',
        'vat_amount' => 'getVatAmount',
        'shipping_country' => 'getShippingCountry',
        'initial_payment_method_type' => 'getInitialPaymentMethodType',
        'initial_number_of_installments' => 'getInitialNumberOfInstallments',
        'initial_payment_method_monthly_downpayments' => 'getInitialPaymentMethodMonthlyDownpayments',
        'merchant_capture_reference' => 'getMerchantCaptureReference',
        'merchant_refund_reference' => 'getMerchantRefundReference',
        'detailed_type' => 'getDetailedType',
        'tax_in_currency_of_registration_country' => 'getTaxInCurrencyOfRegistrationCountry',
        'currency_code_of_registration_country' => 'getCurrencyCodeOfRegistrationCountry'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    const TYPE_COMMISSION = 'COMMISSION';
    const TYPE_SALE = 'SALE';
    const TYPE_REVERSAL = 'REVERSAL';
    const TYPE__RETURN = 'RETURN';
    const TYPE_FEE = 'FEE';
    const TYPE_CREDIT = 'CREDIT';
    const TYPE_CHARGE = 'CHARGE';
    const DETAILED_TYPE_COMMISSION = 'COMMISSION';
    const DETAILED_TYPE_COMMISSION_RETURN = 'COMMISSION_RETURN';
    const DETAILED_TYPE_CREDITED_CORRECTION = 'CREDITED_CORRECTION';
    const DETAILED_TYPE_EXPIRY_FEE_GROSS = 'EXPIRY_FEE_GROSS';
    const DETAILED_TYPE_EXTEND_DUE_DATE_FEE = 'EXTEND_DUE_DATE_FEE';
    const DETAILED_TYPE_FRAUD_POLICY_CHARGE = 'FRAUD_POLICY_CHARGE';
    const DETAILED_TYPE_FRAUD_POLICY_CREDIT_NET = 'FRAUD_POLICY_CREDIT_NET';
    const DETAILED_TYPE_INSUFFICIENT_BANK_DETAILS = 'INSUFFICIENT_BANK_DETAILS';
    const DETAILED_TYPE_LOAN_AMORTISATION = 'LOAN_AMORTISATION';
    const DETAILED_TYPE_LOAN_FEE = 'LOAN_FEE';
    const DETAILED_TYPE_LOAN_PAYOUT = 'LOAN_PAYOUT';
    const DETAILED_TYPE_LATE_RETURN_FEE = 'LATE_RETURN_FEE';
    const DETAILED_TYPE_PURCHASE = 'PURCHASE';
    const DETAILED_TYPE_PURCHASE_COMMISSION_PERCENTAGE = 'PURCHASE_COMMISSION_PERCENTAGE';
    const DETAILED_TYPE_PURCHASE_FEE_FIXED = 'PURCHASE_FEE_FIXED';
    const DETAILED_TYPE_PURCHASE_FEE_PERCENTAGE = 'PURCHASE_FEE_PERCENTAGE';
    const DETAILED_TYPE_PURCHASE_FEE_PERCENTAGE_REFUND = 'PURCHASE_FEE_PERCENTAGE_REFUND';
    const DETAILED_TYPE_PURCHASE_RETURN = 'PURCHASE_RETURN';
    const DETAILED_TYPE_REVERSAL = 'REVERSAL';
    const DETAILED_TYPE_ROLLING_RESERVE = 'ROLLING_RESERVE';
    const DETAILED_TYPE_SERVICING_FEE = 'SERVICING_FEE';
    const DETAILED_TYPE_TRANSFER_FROM_LEGACY_INTEGRATION = 'TRANSFER_FROM_LEGACY_INTEGRATION';
    const DETAILED_TYPE_UNDER_REVIEW = 'UNDER_REVIEW';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTypeAllowableValues()
    {
        return [
            self::TYPE_COMMISSION,
            self::TYPE_SALE,
            self::TYPE_REVERSAL,
            self::TYPE__RETURN,
            self::TYPE_FEE,
            self::TYPE_CREDIT,
            self::TYPE_CHARGE,
        ];
    }

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getDetailedTypeAllowableValues()
    {
        return [
            self::DETAILED_TYPE_COMMISSION,
            self::DETAILED_TYPE_COMMISSION_RETURN,
            self::DETAILED_TYPE_CREDITED_CORRECTION,
            self::DETAILED_TYPE_EXPIRY_FEE_GROSS,
            self::DETAILED_TYPE_EXTEND_DUE_DATE_FEE,
            self::DETAILED_TYPE_FRAUD_POLICY_CHARGE,
            self::DETAILED_TYPE_FRAUD_POLICY_CREDIT_NET,
            self::DETAILED_TYPE_INSUFFICIENT_BANK_DETAILS,
            self::DETAILED_TYPE_LOAN_AMORTISATION,
            self::DETAILED_TYPE_LOAN_FEE,
            self::DETAILED_TYPE_LOAN_PAYOUT,
            self::DETAILED_TYPE_LATE_RETURN_FEE,
            self::DETAILED_TYPE_PURCHASE,
            self::DETAILED_TYPE_PURCHASE_COMMISSION_PERCENTAGE,
            self::DETAILED_TYPE_PURCHASE_FEE_FIXED,
            self::DETAILED_TYPE_PURCHASE_FEE_PERCENTAGE,
            self::DETAILED_TYPE_PURCHASE_FEE_PERCENTAGE_REFUND,
            self::DETAILED_TYPE_PURCHASE_RETURN,
            self::DETAILED_TYPE_REVERSAL,
            self::DETAILED_TYPE_ROLLING_RESERVE,
            self::DETAILED_TYPE_SERVICING_FEE,
            self::DETAILED_TYPE_TRANSFER_FROM_LEGACY_INTEGRATION,
            self::DETAILED_TYPE_UNDER_REVIEW,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['amount'] = $data['amount'] ?? null;
        $this->container['capture_id'] = $data['capture_id'] ?? null;
        $this->container['merchant_reference1'] = $data['merchant_reference1'] ?? null;
        $this->container['sale_date'] = $data['sale_date'] ?? null;
        $this->container['type'] = $data['type'] ?? null;
        $this->container['capture_date'] = $data['capture_date'] ?? null;
        $this->container['payment_reference'] = $data['payment_reference'] ?? null;
        $this->container['order_id'] = $data['order_id'] ?? null;
        $this->container['payout'] = $data['payout'] ?? null;
        $this->container['refund_id'] = $data['refund_id'] ?? null;
        $this->container['short_order_id'] = $data['short_order_id'] ?? null;
        $this->container['merchant_reference2'] = $data['merchant_reference2'] ?? null;
        $this->container['currency_code'] = $data['currency_code'] ?? null;
        $this->container['purchase_country'] = $data['purchase_country'] ?? null;
        $this->container['vat_rate'] = $data['vat_rate'] ?? null;
        $this->container['vat_amount'] = $data['vat_amount'] ?? null;
        $this->container['shipping_country'] = $data['shipping_country'] ?? null;
        $this->container['initial_payment_method_type'] = $data['initial_payment_method_type'] ?? null;
        $this->container['initial_number_of_installments'] = $data['initial_number_of_installments'] ?? null;
        $this->container['initial_payment_method_monthly_downpayments'] = $data['initial_payment_method_monthly_downpayments'] ?? null;
        $this->container['merchant_capture_reference'] = $data['merchant_capture_reference'] ?? null;
        $this->container['merchant_refund_reference'] = $data['merchant_refund_reference'] ?? null;
        $this->container['detailed_type'] = $data['detailed_type'] ?? null;
        $this->container['tax_in_currency_of_registration_country'] = $data['tax_in_currency_of_registration_country'] ?? null;
        $this->container['currency_code_of_registration_country'] = $data['currency_code_of_registration_country'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['amount'] === null) {
            $invalidProperties[] = "'amount' can't be null";
        }
        if ($this->container['capture_id'] === null) {
            $invalidProperties[] = "'capture_id' can't be null";
        }
        if ($this->container['sale_date'] === null) {
            $invalidProperties[] = "'sale_date' can't be null";
        }
        if ($this->container['type'] === null) {
            $invalidProperties[] = "'type' can't be null";
        }
        $allowedValues = $this->getTypeAllowableValues();
        if (!is_null($this->container['type']) && !in_array($this->container['type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'type', must be one of '%s'",
                $this->container['type'],
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['capture_date'] === null) {
            $invalidProperties[] = "'capture_date' can't be null";
        }
        if ($this->container['order_id'] === null) {
            $invalidProperties[] = "'order_id' can't be null";
        }
        if ($this->container['currency_code'] === null) {
            $invalidProperties[] = "'currency_code' can't be null";
        }
        if ($this->container['purchase_country'] === null) {
            $invalidProperties[] = "'purchase_country' can't be null";
        }
        $allowedValues = $this->getDetailedTypeAllowableValues();
        if (!is_null($this->container['detailed_type']) && !in_array($this->container['detailed_type'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'detailed_type', must be one of '%s'",
                $this->container['detailed_type'],
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     *
     * @param int $amount Total amount of the specific transaction, in minor units
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets capture_id
     *
     * @return string
     */
    public function getCaptureId()
    {
        return $this->container['capture_id'];
    }

    /**
     * Sets capture_id
     *
     * @param string $capture_id The Klarna assigned id reference of a specific capture
     *
     * @return self
     */
    public function setCaptureId($capture_id)
    {
        $this->container['capture_id'] = $capture_id;

        return $this;
    }

    /**
     * Gets merchant_reference1
     *
     * @return string|null
     */
    public function getMerchantReference1()
    {
        return $this->container['merchant_reference1'];
    }

    /**
     * Sets merchant_reference1
     *
     * @param string|null $merchant_reference1 Merchant assigned reference, typically a reference to an order management system id
     *
     * @return self
     */
    public function setMerchantReference1($merchant_reference1)
    {
        $this->container['merchant_reference1'] = $merchant_reference1;

        return $this;
    }

    /**
     * Gets sale_date
     *
     * @return \DateTime
     */
    public function getSaleDate()
    {
        return $this->container['sale_date'];
    }

    /**
     * Sets sale_date
     *
     * @param \DateTime $sale_date ISO 8601 formatted date-time string
     *
     * @return self
     */
    public function setSaleDate($sale_date)
    {
        $this->container['sale_date'] = $sale_date;

        return $this;
    }

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     *
     * @param string $type The type of transaction.
     *
     * @return self
     */
    public function setType($type)
    {
        $allowedValues = $this->getTypeAllowableValues();
        if (!in_array($type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'type', must be one of '%s'",
                    $type,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets capture_date
     *
     * @return \DateTime
     */
    public function getCaptureDate()
    {
        return $this->container['capture_date'];
    }

    /**
     * Sets capture_date
     *
     * @param \DateTime $capture_date ISO 8601 formatted date-time string
     *
     * @return self
     */
    public function setCaptureDate($capture_date)
    {
        $this->container['capture_date'] = $capture_date;

        return $this;
    }

    /**
     * Gets payment_reference
     *
     * @return string|null
     */
    public function getPaymentReference()
    {
        return $this->container['payment_reference'];
    }

    /**
     * Sets payment_reference
     *
     * @param string|null $payment_reference Reference to the specific payout the transaction is part of, if available.
     *
     * @return self
     */
    public function setPaymentReference($payment_reference)
    {
        $this->container['payment_reference'] = $payment_reference;

        return $this;
    }

    /**
     * Gets order_id
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->container['order_id'];
    }

    /**
     * Sets order_id
     *
     * @param string $order_id The Klarna assigned order id reference
     *
     * @return self
     */
    public function setOrderId($order_id)
    {
        $this->container['order_id'] = $order_id;

        return $this;
    }

    /**
     * Gets payout
     *
     * @return string|null
     */
    public function getPayout()
    {
        return $this->container['payout'];
    }

    /**
     * Sets payout
     *
     * @param string|null $payout Link to the payout that this transaction is part of
     *
     * @return self
     */
    public function setPayout($payout)
    {
        $this->container['payout'] = $payout;

        return $this;
    }

    /**
     * Gets refund_id
     *
     * @return string|null
     */
    public function getRefundId()
    {
        return $this->container['refund_id'];
    }

    /**
     * Sets refund_id
     *
     * @param string|null $refund_id The Klarna assigned id reference of a specific refund
     *
     * @return self
     */
    public function setRefundId($refund_id)
    {
        $this->container['refund_id'] = $refund_id;

        return $this;
    }

    /**
     * Gets short_order_id
     *
     * @return string|null
     */
    public function getShortOrderId()
    {
        return $this->container['short_order_id'];
    }

    /**
     * Sets short_order_id
     *
     * @param string|null $short_order_id The Klarna assigned short order id reference
     *
     * @return self
     */
    public function setShortOrderId($short_order_id)
    {
        $this->container['short_order_id'] = $short_order_id;

        return $this;
    }

    /**
     * Gets merchant_reference2
     *
     * @return string|null
     */
    public function getMerchantReference2()
    {
        return $this->container['merchant_reference2'];
    }

    /**
     * Sets merchant_reference2
     *
     * @param string|null $merchant_reference2 Merchant assigned reference, typically a reference to an order management system id
     *
     * @return self
     */
    public function setMerchantReference2($merchant_reference2)
    {
        $this->container['merchant_reference2'] = $merchant_reference2;

        return $this;
    }

    /**
     * Gets currency_code
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->container['currency_code'];
    }

    /**
     * Sets currency_code
     *
     * @param string $currency_code ISO 4217 Currency Code. Like USD, EUR, AUD or GBP.
     *
     * @return self
     */
    public function setCurrencyCode($currency_code)
    {
        $this->container['currency_code'] = $currency_code;

        return $this;
    }

    /**
     * Gets purchase_country
     *
     * @return string
     */
    public function getPurchaseCountry()
    {
        return $this->container['purchase_country'];
    }

    /**
     * Sets purchase_country
     *
     * @param string $purchase_country ISO Alpha-2 Country Code
     *
     * @return self
     */
    public function setPurchaseCountry($purchase_country)
    {
        $this->container['purchase_country'] = $purchase_country;

        return $this;
    }

    /**
     * Gets vat_rate
     *
     * @return int|null
     */
    public function getVatRate()
    {
        return $this->container['vat_rate'];
    }

    /**
     * Sets vat_rate
     *
     * @param int|null $vat_rate VAT (Value added tax) rate on Klarna fees
     *
     * @return self
     */
    public function setVatRate($vat_rate)
    {
        $this->container['vat_rate'] = $vat_rate;

        return $this;
    }

    /**
     * Gets vat_amount
     *
     * @return int|null
     */
    public function getVatAmount()
    {
        return $this->container['vat_amount'];
    }

    /**
     * Sets vat_amount
     *
     * @param int|null $vat_amount VAT (Value added tax) amount on Klarna fees, in minor units
     *
     * @return self
     */
    public function setVatAmount($vat_amount)
    {
        $this->container['vat_amount'] = $vat_amount;

        return $this;
    }

    /**
     * Gets shipping_country
     *
     * @return string|null
     */
    public function getShippingCountry()
    {
        return $this->container['shipping_country'];
    }

    /**
     * Sets shipping_country
     *
     * @param string|null $shipping_country ISO Alpha-2 Country Code
     *
     * @return self
     */
    public function setShippingCountry($shipping_country)
    {
        $this->container['shipping_country'] = $shipping_country;

        return $this;
    }

    /**
     * Gets initial_payment_method_type
     *
     * @return string|null
     */
    public function getInitialPaymentMethodType()
    {
        return $this->container['initial_payment_method_type'];
    }

    /**
     * Sets initial_payment_method_type
     *
     * @param string|null $initial_payment_method_type Payment method the consumer chose during checkout
     *
     * @return self
     */
    public function setInitialPaymentMethodType($initial_payment_method_type)
    {
        $this->container['initial_payment_method_type'] = $initial_payment_method_type;

        return $this;
    }

    /**
     * Gets initial_number_of_installments
     *
     * @return int|null
     */
    public function getInitialNumberOfInstallments()
    {
        return $this->container['initial_number_of_installments'];
    }

    /**
     * Sets initial_number_of_installments
     *
     * @param int|null $initial_number_of_installments Number of installments the consumer chose during checkout in case of installment payments
     *
     * @return self
     */
    public function setInitialNumberOfInstallments($initial_number_of_installments)
    {
        $this->container['initial_number_of_installments'] = $initial_number_of_installments;

        return $this;
    }

    /**
     * Gets initial_payment_method_monthly_downpayments
     *
     * @return int|null
     */
    public function getInitialPaymentMethodMonthlyDownpayments()
    {
        return $this->container['initial_payment_method_monthly_downpayments'];
    }

    /**
     * Sets initial_payment_method_monthly_downpayments
     *
     * @param int|null $initial_payment_method_monthly_downpayments Number of monthly downpayments that were chosen during the checkout in case of installment payments.
     *
     * @return self
     */
    public function setInitialPaymentMethodMonthlyDownpayments($initial_payment_method_monthly_downpayments)
    {
        $this->container['initial_payment_method_monthly_downpayments'] = $initial_payment_method_monthly_downpayments;

        return $this;
    }

    /**
     * Gets merchant_capture_reference
     *
     * @return string|null
     */
    public function getMerchantCaptureReference()
    {
        return $this->container['merchant_capture_reference'];
    }

    /**
     * Sets merchant_capture_reference
     *
     * @param string|null $merchant_capture_reference Your internal reference to the capture, that has been submitted during capturing an order via API
     *
     * @return self
     */
    public function setMerchantCaptureReference($merchant_capture_reference)
    {
        $this->container['merchant_capture_reference'] = $merchant_capture_reference;

        return $this;
    }

    /**
     * Gets merchant_refund_reference
     *
     * @return string|null
     */
    public function getMerchantRefundReference()
    {
        return $this->container['merchant_refund_reference'];
    }

    /**
     * Sets merchant_refund_reference
     *
     * @param string|null $merchant_refund_reference Your internal reference to the refund, that has been submitted during refunding an order via API
     *
     * @return self
     */
    public function setMerchantRefundReference($merchant_refund_reference)
    {
        $this->container['merchant_refund_reference'] = $merchant_refund_reference;

        return $this;
    }

    /**
     * Gets detailed_type
     *
     * @return string|null
     */
    public function getDetailedType()
    {
        return $this->container['detailed_type'];
    }

    /**
     * Sets detailed_type
     *
     * @param string|null $detailed_type Detailed description of the transaction type
     *
     * @return self
     */
    public function setDetailedType($detailed_type)
    {
        $allowedValues = $this->getDetailedTypeAllowableValues();
        if (!is_null($detailed_type) && !in_array($detailed_type, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'detailed_type', must be one of '%s'",
                    $detailed_type,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['detailed_type'] = $detailed_type;

        return $this;
    }

    /**
     * Gets tax_in_currency_of_registration_country
     *
     * @return int|null
     */
    public function getTaxInCurrencyOfRegistrationCountry()
    {
        return $this->container['tax_in_currency_of_registration_country'];
    }

    /**
     * Sets tax_in_currency_of_registration_country
     *
     * @param int|null $tax_in_currency_of_registration_country The tax amount on the respective fee, converted into the currency of your registration country. In case you are a German merchant selling in another currency then EUR or a Swedish merchant selling in another currency then SEK, we convert the VAT amount on the Klarna fees into the currency of the country you are registered in, based on the exchange rate of the capture date.
     *
     * @return self
     */
    public function setTaxInCurrencyOfRegistrationCountry($tax_in_currency_of_registration_country)
    {
        $this->container['tax_in_currency_of_registration_country'] = $tax_in_currency_of_registration_country;

        return $this;
    }

    /**
     * Gets currency_code_of_registration_country
     *
     * @return string|null
     */
    public function getCurrencyCodeOfRegistrationCountry()
    {
        return $this->container['currency_code_of_registration_country'];
    }

    /**
     * Sets currency_code_of_registration_country
     *
     * @param string|null $currency_code_of_registration_country ISO 4217 Currency Code of the country you are registered in.
     *
     * @return self
     */
    public function setCurrencyCodeOfRegistrationCountry($currency_code_of_registration_country)
    {
        $this->container['currency_code_of_registration_country'] = $currency_code_of_registration_country;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


