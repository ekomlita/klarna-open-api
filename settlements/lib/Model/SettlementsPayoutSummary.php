<?php
/**
 * SettlementsPayoutSummary
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Settlements API
 *
 * The Settlements API helps you with the reconciliation of payments, made by Klarna to your bank account. Every payment has a unique payment_reference that can be found in the settlement reports and on your bank statement.  Besides responses in JSON-format, we provide endpoints to download transactional CSV reports, as well as PDF Summary, reports. Both reports can be downloaded for a specific payout as well as aggregated for a time range.  ![](https://images.prismic.io/docsportal/b20b8140-ea4e-4b0a-a812-dfb459aa332d_FIRE-settlements_api_overview.png?auto=compress,format)  For the day-to-day reconciliation of payments we recommend the following:  - By performing the requests “get all payouts” you will get the totals of the payouts and the corresponding payment references as a response. The payment references are needed for the request “get transactions”.  - Perform the request \"get transactions\". The response contains all related transactions of a payout.  - If you prefer to use CSV-files for your reconciliation, you can query “Get CSV payout report” for a specific payment_reference.  Every transaction is related to a unique order_ID. All related transactions in the life-span of an order are associated with this ID. eg. fees or refunds. It is, therefore, the recommended identifier for reconciling the report lines with your system.  Resources are split into two broad types:  - Collections, including pagination information: collections are queryable, typically by the attributes of the sub-resource as well as pagination.  - Entity resources containing a single entity.
 *
 * The version of the OpenAPI document: 1.0.0-rc2
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace KlarnaSettlementsApi\Model;

use \ArrayAccess;
use \KlarnaSettlementsApi\ObjectSerializer;

/**
 * SettlementsPayoutSummary Class Doc Comment
 *
 * @category Class
 * @package  KlarnaSettlementsApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class SettlementsPayoutSummary implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'PayoutSummary';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'summary_total_fee_correction_amount' => 'int',
        'summary_payout_date_start' => '\DateTime',
        'summary_total_release_amount' => 'int',
        'summary_settlement_currency' => 'string',
        'summary_payout_date_end' => '\DateTime',
        'summary_total_tax_amount' => 'int',
        'summary_total_settlement_amount' => 'int',
        'summary_total_holdback_amount' => 'int',
        'summary_total_reversal_amount' => 'int',
        'summary_total_return_amount' => 'int',
        'summary_total_fee_amount' => 'int',
        'summary_total_commission_amount' => 'int',
        'summary_total_sale_amount' => 'int',
        'summary_total_repay_amount' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'summary_total_fee_correction_amount' => 'int64',
        'summary_payout_date_start' => 'date-time',
        'summary_total_release_amount' => 'int64',
        'summary_settlement_currency' => null,
        'summary_payout_date_end' => 'date-time',
        'summary_total_tax_amount' => 'int64',
        'summary_total_settlement_amount' => 'int64',
        'summary_total_holdback_amount' => 'int64',
        'summary_total_reversal_amount' => 'int64',
        'summary_total_return_amount' => 'int64',
        'summary_total_fee_amount' => 'int64',
        'summary_total_commission_amount' => 'int64',
        'summary_total_sale_amount' => 'int64',
        'summary_total_repay_amount' => 'int64'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'summary_total_fee_correction_amount' => 'summary_total_fee_correction_amount',
        'summary_payout_date_start' => 'summary_payout_date_start',
        'summary_total_release_amount' => 'summary_total_release_amount',
        'summary_settlement_currency' => 'summary_settlement_currency',
        'summary_payout_date_end' => 'summary_payout_date_end',
        'summary_total_tax_amount' => 'summary_total_tax_amount',
        'summary_total_settlement_amount' => 'summary_total_settlement_amount',
        'summary_total_holdback_amount' => 'summary_total_holdback_amount',
        'summary_total_reversal_amount' => 'summary_total_reversal_amount',
        'summary_total_return_amount' => 'summary_total_return_amount',
        'summary_total_fee_amount' => 'summary_total_fee_amount',
        'summary_total_commission_amount' => 'summary_total_commission_amount',
        'summary_total_sale_amount' => 'summary_total_sale_amount',
        'summary_total_repay_amount' => 'summary_total_repay_amount'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'summary_total_fee_correction_amount' => 'setSummaryTotalFeeCorrectionAmount',
        'summary_payout_date_start' => 'setSummaryPayoutDateStart',
        'summary_total_release_amount' => 'setSummaryTotalReleaseAmount',
        'summary_settlement_currency' => 'setSummarySettlementCurrency',
        'summary_payout_date_end' => 'setSummaryPayoutDateEnd',
        'summary_total_tax_amount' => 'setSummaryTotalTaxAmount',
        'summary_total_settlement_amount' => 'setSummaryTotalSettlementAmount',
        'summary_total_holdback_amount' => 'setSummaryTotalHoldbackAmount',
        'summary_total_reversal_amount' => 'setSummaryTotalReversalAmount',
        'summary_total_return_amount' => 'setSummaryTotalReturnAmount',
        'summary_total_fee_amount' => 'setSummaryTotalFeeAmount',
        'summary_total_commission_amount' => 'setSummaryTotalCommissionAmount',
        'summary_total_sale_amount' => 'setSummaryTotalSaleAmount',
        'summary_total_repay_amount' => 'setSummaryTotalRepayAmount'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'summary_total_fee_correction_amount' => 'getSummaryTotalFeeCorrectionAmount',
        'summary_payout_date_start' => 'getSummaryPayoutDateStart',
        'summary_total_release_amount' => 'getSummaryTotalReleaseAmount',
        'summary_settlement_currency' => 'getSummarySettlementCurrency',
        'summary_payout_date_end' => 'getSummaryPayoutDateEnd',
        'summary_total_tax_amount' => 'getSummaryTotalTaxAmount',
        'summary_total_settlement_amount' => 'getSummaryTotalSettlementAmount',
        'summary_total_holdback_amount' => 'getSummaryTotalHoldbackAmount',
        'summary_total_reversal_amount' => 'getSummaryTotalReversalAmount',
        'summary_total_return_amount' => 'getSummaryTotalReturnAmount',
        'summary_total_fee_amount' => 'getSummaryTotalFeeAmount',
        'summary_total_commission_amount' => 'getSummaryTotalCommissionAmount',
        'summary_total_sale_amount' => 'getSummaryTotalSaleAmount',
        'summary_total_repay_amount' => 'getSummaryTotalRepayAmount'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['summary_total_fee_correction_amount'] = $data['summary_total_fee_correction_amount'] ?? null;
        $this->container['summary_payout_date_start'] = $data['summary_payout_date_start'] ?? null;
        $this->container['summary_total_release_amount'] = $data['summary_total_release_amount'] ?? null;
        $this->container['summary_settlement_currency'] = $data['summary_settlement_currency'] ?? null;
        $this->container['summary_payout_date_end'] = $data['summary_payout_date_end'] ?? null;
        $this->container['summary_total_tax_amount'] = $data['summary_total_tax_amount'] ?? null;
        $this->container['summary_total_settlement_amount'] = $data['summary_total_settlement_amount'] ?? null;
        $this->container['summary_total_holdback_amount'] = $data['summary_total_holdback_amount'] ?? null;
        $this->container['summary_total_reversal_amount'] = $data['summary_total_reversal_amount'] ?? null;
        $this->container['summary_total_return_amount'] = $data['summary_total_return_amount'] ?? null;
        $this->container['summary_total_fee_amount'] = $data['summary_total_fee_amount'] ?? null;
        $this->container['summary_total_commission_amount'] = $data['summary_total_commission_amount'] ?? null;
        $this->container['summary_total_sale_amount'] = $data['summary_total_sale_amount'] ?? null;
        $this->container['summary_total_repay_amount'] = $data['summary_total_repay_amount'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['summary_total_fee_correction_amount'] === null) {
            $invalidProperties[] = "'summary_total_fee_correction_amount' can't be null";
        }
        if ($this->container['summary_payout_date_start'] === null) {
            $invalidProperties[] = "'summary_payout_date_start' can't be null";
        }
        if ($this->container['summary_total_release_amount'] === null) {
            $invalidProperties[] = "'summary_total_release_amount' can't be null";
        }
        if ($this->container['summary_settlement_currency'] === null) {
            $invalidProperties[] = "'summary_settlement_currency' can't be null";
        }
        if ($this->container['summary_payout_date_end'] === null) {
            $invalidProperties[] = "'summary_payout_date_end' can't be null";
        }
        if ($this->container['summary_total_tax_amount'] === null) {
            $invalidProperties[] = "'summary_total_tax_amount' can't be null";
        }
        if ($this->container['summary_total_settlement_amount'] === null) {
            $invalidProperties[] = "'summary_total_settlement_amount' can't be null";
        }
        if ($this->container['summary_total_holdback_amount'] === null) {
            $invalidProperties[] = "'summary_total_holdback_amount' can't be null";
        }
        if ($this->container['summary_total_reversal_amount'] === null) {
            $invalidProperties[] = "'summary_total_reversal_amount' can't be null";
        }
        if ($this->container['summary_total_return_amount'] === null) {
            $invalidProperties[] = "'summary_total_return_amount' can't be null";
        }
        if ($this->container['summary_total_fee_amount'] === null) {
            $invalidProperties[] = "'summary_total_fee_amount' can't be null";
        }
        if ($this->container['summary_total_commission_amount'] === null) {
            $invalidProperties[] = "'summary_total_commission_amount' can't be null";
        }
        if ($this->container['summary_total_sale_amount'] === null) {
            $invalidProperties[] = "'summary_total_sale_amount' can't be null";
        }
        if ($this->container['summary_total_repay_amount'] === null) {
            $invalidProperties[] = "'summary_total_repay_amount' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets summary_total_fee_correction_amount
     *
     * @return int
     */
    public function getSummaryTotalFeeCorrectionAmount()
    {
        return $this->container['summary_total_fee_correction_amount'];
    }

    /**
     * Sets summary_total_fee_correction_amount
     *
     * @param int $summary_total_fee_correction_amount The total amount of fee correction, in minor units
     *
     * @return self
     */
    public function setSummaryTotalFeeCorrectionAmount($summary_total_fee_correction_amount)
    {
        $this->container['summary_total_fee_correction_amount'] = $summary_total_fee_correction_amount;

        return $this;
    }

    /**
     * Gets summary_payout_date_start
     *
     * @return \DateTime
     */
    public function getSummaryPayoutDateStart()
    {
        return $this->container['summary_payout_date_start'];
    }

    /**
     * Sets summary_payout_date_start
     *
     * @param \DateTime $summary_payout_date_start ISO 8601 formatted date-time string
     *
     * @return self
     */
    public function setSummaryPayoutDateStart($summary_payout_date_start)
    {
        $this->container['summary_payout_date_start'] = $summary_payout_date_start;

        return $this;
    }

    /**
     * Gets summary_total_release_amount
     *
     * @return int
     */
    public function getSummaryTotalReleaseAmount()
    {
        return $this->container['summary_total_release_amount'];
    }

    /**
     * Sets summary_total_release_amount
     *
     * @param int $summary_total_release_amount The total amount of money released from holdback by Klarna, in minor units
     *
     * @return self
     */
    public function setSummaryTotalReleaseAmount($summary_total_release_amount)
    {
        $this->container['summary_total_release_amount'] = $summary_total_release_amount;

        return $this;
    }

    /**
     * Gets summary_settlement_currency
     *
     * @return string
     */
    public function getSummarySettlementCurrency()
    {
        return $this->container['summary_settlement_currency'];
    }

    /**
     * Sets summary_settlement_currency
     *
     * @param string $summary_settlement_currency ISO 4217 Currency Code. Like USD, EUR, AUD or GBP.
     *
     * @return self
     */
    public function setSummarySettlementCurrency($summary_settlement_currency)
    {
        $this->container['summary_settlement_currency'] = $summary_settlement_currency;

        return $this;
    }

    /**
     * Gets summary_payout_date_end
     *
     * @return \DateTime
     */
    public function getSummaryPayoutDateEnd()
    {
        return $this->container['summary_payout_date_end'];
    }

    /**
     * Sets summary_payout_date_end
     *
     * @param \DateTime $summary_payout_date_end ISO 8601 formatted date-time string
     *
     * @return self
     */
    public function setSummaryPayoutDateEnd($summary_payout_date_end)
    {
        $this->container['summary_payout_date_end'] = $summary_payout_date_end;

        return $this;
    }

    /**
     * Gets summary_total_tax_amount
     *
     * @return int
     */
    public function getSummaryTotalTaxAmount()
    {
        return $this->container['summary_total_tax_amount'];
    }

    /**
     * Sets summary_total_tax_amount
     *
     * @param int $summary_total_tax_amount The total amount of tax, in minor units
     *
     * @return self
     */
    public function setSummaryTotalTaxAmount($summary_total_tax_amount)
    {
        $this->container['summary_total_tax_amount'] = $summary_total_tax_amount;

        return $this;
    }

    /**
     * Gets summary_total_settlement_amount
     *
     * @return int
     */
    public function getSummaryTotalSettlementAmount()
    {
        return $this->container['summary_total_settlement_amount'];
    }

    /**
     * Sets summary_total_settlement_amount
     *
     * @param int $summary_total_settlement_amount The total amount of the settlement in question, in minor units
     *
     * @return self
     */
    public function setSummaryTotalSettlementAmount($summary_total_settlement_amount)
    {
        $this->container['summary_total_settlement_amount'] = $summary_total_settlement_amount;

        return $this;
    }

    /**
     * Gets summary_total_holdback_amount
     *
     * @return int
     */
    public function getSummaryTotalHoldbackAmount()
    {
        return $this->container['summary_total_holdback_amount'];
    }

    /**
     * Sets summary_total_holdback_amount
     *
     * @param int $summary_total_holdback_amount The total amount of money withheld by Klarna, in minor units
     *
     * @return self
     */
    public function setSummaryTotalHoldbackAmount($summary_total_holdback_amount)
    {
        $this->container['summary_total_holdback_amount'] = $summary_total_holdback_amount;

        return $this;
    }

    /**
     * Gets summary_total_reversal_amount
     *
     * @return int
     */
    public function getSummaryTotalReversalAmount()
    {
        return $this->container['summary_total_reversal_amount'];
    }

    /**
     * Sets summary_total_reversal_amount
     *
     * @param int $summary_total_reversal_amount The total amount of reversals, in minor units
     *
     * @return self
     */
    public function setSummaryTotalReversalAmount($summary_total_reversal_amount)
    {
        $this->container['summary_total_reversal_amount'] = $summary_total_reversal_amount;

        return $this;
    }

    /**
     * Gets summary_total_return_amount
     *
     * @return int
     */
    public function getSummaryTotalReturnAmount()
    {
        return $this->container['summary_total_return_amount'];
    }

    /**
     * Sets summary_total_return_amount
     *
     * @param int $summary_total_return_amount The total amount of returns, in minor units
     *
     * @return self
     */
    public function setSummaryTotalReturnAmount($summary_total_return_amount)
    {
        $this->container['summary_total_return_amount'] = $summary_total_return_amount;

        return $this;
    }

    /**
     * Gets summary_total_fee_amount
     *
     * @return int
     */
    public function getSummaryTotalFeeAmount()
    {
        return $this->container['summary_total_fee_amount'];
    }

    /**
     * Sets summary_total_fee_amount
     *
     * @param int $summary_total_fee_amount The total amount of fees, in minor units
     *
     * @return self
     */
    public function setSummaryTotalFeeAmount($summary_total_fee_amount)
    {
        $this->container['summary_total_fee_amount'] = $summary_total_fee_amount;

        return $this;
    }

    /**
     * Gets summary_total_commission_amount
     *
     * @return int
     */
    public function getSummaryTotalCommissionAmount()
    {
        return $this->container['summary_total_commission_amount'];
    }

    /**
     * Sets summary_total_commission_amount
     *
     * @param int $summary_total_commission_amount The total amount of commissions, in minor units
     *
     * @return self
     */
    public function setSummaryTotalCommissionAmount($summary_total_commission_amount)
    {
        $this->container['summary_total_commission_amount'] = $summary_total_commission_amount;

        return $this;
    }

    /**
     * Gets summary_total_sale_amount
     *
     * @return int
     */
    public function getSummaryTotalSaleAmount()
    {
        return $this->container['summary_total_sale_amount'];
    }

    /**
     * Sets summary_total_sale_amount
     *
     * @param int $summary_total_sale_amount The total amount of sales, in minor units
     *
     * @return self
     */
    public function setSummaryTotalSaleAmount($summary_total_sale_amount)
    {
        $this->container['summary_total_sale_amount'] = $summary_total_sale_amount;

        return $this;
    }

    /**
     * Gets summary_total_repay_amount
     *
     * @return int
     */
    public function getSummaryTotalRepayAmount()
    {
        return $this->container['summary_total_repay_amount'];
    }

    /**
     * Sets summary_total_repay_amount
     *
     * @param int $summary_total_repay_amount The total amount of money that has been repaid by the merchant from the debt to Klarna, in minor units
     *
     * @return self
     */
    public function setSummaryTotalRepayAmount($summary_total_repay_amount)
    {
        $this->container['summary_total_repay_amount'] = $summary_total_repay_amount;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


