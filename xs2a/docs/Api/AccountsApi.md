# KlarnaXsAApi\AccountsApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountBalanceFlow()**](AccountsApi.md#accountBalanceFlow) | **GET** /v2/accounts/{account_id}/balance | Get account balance.
[**accountFlow()**](AccountsApi.md#accountFlow) | **GET** /v2/accounts/{account_id} | Get account details.
[**accountTransactionsFlow()**](AccountsApi.md#accountTransactionsFlow) | **GET** /v2/accounts/{account_id}/transactions | Get paginated transaction list.
[**accountsFlow()**](AccountsApi.md#accountsFlow) | **GET** /v2/accounts | Get account list.
[**fundFlow()**](AccountsApi.md#fundFlow) | **POST** /v2/funds-confirmations | Confirm available funds.


## `accountBalanceFlow()`

```php
accountBalanceFlow($consent_id, $account_id, $x_request_id): \KlarnaXsAApi\Model\XsABalance
```

Get account balance.

Get the account balance for a given account.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$consent_id = 'consent_id_example'; // string | The consent-id that was received in the GET /consent-sessions/{consent_session_id} request
$account_id = 'account_id_example'; // string | account id
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $result = $apiInstance->accountBalanceFlow($consent_id, $account_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->accountBalanceFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consent_id** | **string**| The consent-id that was received in the GET /consent-sessions/{consent_session_id} request |
 **account_id** | **string**| account id |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsABalance**](../Model/XsABalance.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountFlow()`

```php
accountFlow($consent_id, $account_id, $x_request_id): \KlarnaXsAApi\Model\XsAAccount
```

Get account details.

Get the account details of a given account.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$consent_id = 'consent_id_example'; // string | The consent-id that was received in the GET /consent-sessions/{consent_session_id} request
$account_id = 'account_id_example'; // string | account id
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $result = $apiInstance->accountFlow($consent_id, $account_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->accountFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consent_id** | **string**| The consent-id that was received in the GET /consent-sessions/{consent_session_id} request |
 **account_id** | **string**| account id |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsAAccount**](../Model/XsAAccount.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountTransactionsFlow()`

```php
accountTransactionsFlow($consent_id, $account_id, $x_request_id, $start_at, $end_at, $page_number, $page_size): \KlarnaXsAApi\Model\XsATransactionsResponse
```

Get paginated transaction list.

Get the transactions for a given account.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$consent_id = 'consent_id_example'; // string | The consent-id that was received in the GET /consent-sessions/{consent_session_id} request
$account_id = 'account_id_example'; // string | account id
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.
$start_at = 'start_at_example'; // string | Format: `yyyy-MM-dd` Earliest inclusive initiation or completion date of a transaction.
$end_at = 'end_at_example'; // string | Format: `yyyy-MM-dd` Latest inclusive initiation or completion date of a transaction.
$page_number = 0; // int | Page number - defaults to 0
$page_size = 20; // int | Page size - defaults to 20

try {
    $result = $apiInstance->accountTransactionsFlow($consent_id, $account_id, $x_request_id, $start_at, $end_at, $page_number, $page_size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->accountTransactionsFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consent_id** | **string**| The consent-id that was received in the GET /consent-sessions/{consent_session_id} request |
 **account_id** | **string**| account id |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]
 **start_at** | **string**| Format: &#x60;yyyy-MM-dd&#x60; Earliest inclusive initiation or completion date of a transaction. | [optional]
 **end_at** | **string**| Format: &#x60;yyyy-MM-dd&#x60; Latest inclusive initiation or completion date of a transaction. | [optional]
 **page_number** | **int**| Page number - defaults to 0 | [optional] [default to 0]
 **page_size** | **int**| Page size - defaults to 20 | [optional] [default to 20]

### Return type

[**\KlarnaXsAApi\Model\XsATransactionsResponse**](../Model/XsATransactionsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountsFlow()`

```php
accountsFlow($consent_id, $x_request_id): \KlarnaXsAApi\Model\XsAAccounts
```

Get account list.

Get the list of accounts of a given user.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$consent_id = 'consent_id_example'; // string | The consent-id that was received in the GET /consent-sessions/{consent_session_id} request
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $result = $apiInstance->accountsFlow($consent_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->accountsFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consent_id** | **string**| The consent-id that was received in the GET /consent-sessions/{consent_session_id} request |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsAAccounts**](../Model/XsAAccounts.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `fundFlow()`

```php
fundFlow($consent_id, $x_request_id, $xs_a_post_funds_confirmation_request_body): \KlarnaXsAApi\Model\XsAFundsConfirmation
```

Confirm available funds.

Confirm of the available funds for an amount and a given email in a given currency.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$consent_id = 'consent_id_example'; // string | The consent-id that was received in the GET /consent-sessions/{consent_session_id} request
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.
$xs_a_post_funds_confirmation_request_body = new \KlarnaXsAApi\Model\XsAPostFundsConfirmationRequestBody(); // \KlarnaXsAApi\Model\XsAPostFundsConfirmationRequestBody

try {
    $result = $apiInstance->fundFlow($consent_id, $x_request_id, $xs_a_post_funds_confirmation_request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->fundFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consent_id** | **string**| The consent-id that was received in the GET /consent-sessions/{consent_session_id} request |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]
 **xs_a_post_funds_confirmation_request_body** | [**\KlarnaXsAApi\Model\XsAPostFundsConfirmationRequestBody**](../Model/XsAPostFundsConfirmationRequestBody.md)|  | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsAFundsConfirmation**](../Model/XsAFundsConfirmation.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
