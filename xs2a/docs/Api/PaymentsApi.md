# KlarnaXsAApi\PaymentsApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**deletePaymentFlow()**](PaymentsApi.md#deletePaymentFlow) | **DELETE** /v2/payments/{payment_id} | Revoke a payment
[**getPaymentFlow()**](PaymentsApi.md#getPaymentFlow) | **GET** /v2/payments/{payment_id} | Get payment details.
[**paymentFlow()**](PaymentsApi.md#paymentFlow) | **POST** /v2/payments | Create payment.


## `deletePaymentFlow()`

```php
deletePaymentFlow($payment_id, $x_request_id)
```

Revoke a payment

This will only revoke payments with status AWAITING_APPROVAL

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\PaymentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_id = 'payment_id_example'; // string | ID of the payment.
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $apiInstance->deletePaymentFlow($payment_id, $x_request_id);
} catch (Exception $e) {
    echo 'Exception when calling PaymentsApi->deletePaymentFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_id** | **string**| ID of the payment. |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPaymentFlow()`

```php
getPaymentFlow($payment_id, $x_request_id): \KlarnaXsAApi\Model\XsAGetPaymentResponse
```

Get payment details.

Get the payment details of a given payment.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\PaymentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$payment_id = 'payment_id_example'; // string | ID of the payment.
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $result = $apiInstance->getPaymentFlow($payment_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentsApi->getPaymentFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_id** | **string**| ID of the payment. |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsAGetPaymentResponse**](../Model/XsAGetPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `paymentFlow()`

```php
paymentFlow($x_request_id, $xs_a_post_payment_request_body): \KlarnaXsAApi\Model\XsAPostPaymentResponse
```

Create payment.

Initiate a standard or standing order payment.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\PaymentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.
$xs_a_post_payment_request_body = new \KlarnaXsAApi\Model\XsAPostPaymentRequestBody(); // \KlarnaXsAApi\Model\XsAPostPaymentRequestBody

try {
    $result = $apiInstance->paymentFlow($x_request_id, $xs_a_post_payment_request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentsApi->paymentFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]
 **xs_a_post_payment_request_body** | [**\KlarnaXsAApi\Model\XsAPostPaymentRequestBody**](../Model/XsAPostPaymentRequestBody.md)|  | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsAPostPaymentResponse**](../Model/XsAPostPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
