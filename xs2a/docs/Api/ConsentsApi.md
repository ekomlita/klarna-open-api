# KlarnaXsAApi\ConsentsApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**consentFlow()**](ConsentsApi.md#consentFlow) | **POST** /v2/consent-sessions | Create consent session.
[**getConsentFlow()**](ConsentsApi.md#getConsentFlow) | **GET** /v2/consent-sessions/{consent_session_id} | Get consent session.


## `consentFlow()`

```php
consentFlow($xs_a_post_consent_session_request_body, $x_request_id): \KlarnaXsAApi\Model\XsAPostConsentSessionResponse
```

Create consent session.

Create a consent session by providing email and permissions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\ConsentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$xs_a_post_consent_session_request_body = new \KlarnaXsAApi\Model\XsAPostConsentSessionRequestBody(); // \KlarnaXsAApi\Model\XsAPostConsentSessionRequestBody
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $result = $apiInstance->consentFlow($xs_a_post_consent_session_request_body, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConsentsApi->consentFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xs_a_post_consent_session_request_body** | [**\KlarnaXsAApi\Model\XsAPostConsentSessionRequestBody**](../Model/XsAPostConsentSessionRequestBody.md)|  |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsAPostConsentSessionResponse**](../Model/XsAPostConsentSessionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getConsentFlow()`

```php
getConsentFlow($consent_session_id, $x_request_id): \KlarnaXsAApi\Model\XsAGetConsentSessionResponse
```

Get consent session.

Get the status of a consent session.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaXsAApi\Api\ConsentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$consent_session_id = 'consent_session_id_example'; // string | ID of the consent session.
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $result = $apiInstance->getConsentFlow($consent_session_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConsentsApi->getConsentFlow: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **consent_session_id** | **string**| ID of the consent session. |
 **x_request_id** | **string**| ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests. | [optional]

### Return type

[**\KlarnaXsAApi\Model\XsAGetConsentSessionResponse**](../Model/XsAGetConsentSessionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
