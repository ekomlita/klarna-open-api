# # XsAPostPaymentRequestBodyTo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iban** | **string** | IBAN of the other party bank. |
**bic** | **string** | BIC of the other party bank. |
**recipient_name** | **string** | Other party&#39;s name or information. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
