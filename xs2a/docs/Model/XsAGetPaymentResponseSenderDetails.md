# # XsAGetPaymentResponseSenderDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the sender, or null if payment is not APPROVED. | [optional]
**iban** | **string** | IBAN of the sender&#39;s account, or null if payment is not APPROVED. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
