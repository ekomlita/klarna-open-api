# # XsAPostPaymentRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** | Payment amount. |
**currency** | **string** | Payment currency. |
**email** | **string** | Email of the payment creator. |
**postal_code** | **string** | Postal code of the payment creator. |
**reference** | **string** | Payment reference. | [optional]
**to** | [**\KlarnaXsAApi\Model\XsAPostPaymentRequestBodyTo**](XsAPostPaymentRequestBodyTo.md) |  |
**standing_order** | [**\KlarnaXsAApi\Model\XsAPostPaymentRequestBodyStandingOrder**](XsAPostPaymentRequestBodyStandingOrder.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
