# # XsAPostConsentSessionRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Email address the user used for their Klarna Bank Account. |
**postal_code** | **string** | Postal code of the consent session creator. |
**permission** | **string[]** | Type of PSD2 permission level to grant. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
