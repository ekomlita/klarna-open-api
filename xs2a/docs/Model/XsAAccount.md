# # XsAAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID of the account. | [optional]
**type** | **string** | Type of the account. | [optional]
**account_holder_id** | **string** | ID of the account holder. | [optional]
**account_number** | **string** | Account number. | [optional]
**iban** | **string** | IBAN number of the account. | [optional]
**bic** | **string** | BIC number of the account. | [optional]
**holder_name** | **string** | Name or information of the Klarna Bank Account owner. | [optional]
**holder_address** | **string** | Address of the Klarna Bank Account owner. | [optional]
**bank_name** | **string** | Name or information of the bank. | [optional]
**bank_address** | **string** | Address of the bank. | [optional]
**sort_code** | **string** | Klarna sort code. | [optional]
**balance** | [**\KlarnaXsAApi\Model\XsABalance**](XsABalance.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
