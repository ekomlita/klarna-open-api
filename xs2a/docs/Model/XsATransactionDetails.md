# # XsATransactionDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Transaction type |
**iban** | **string** | IBAN of the other party bank | [optional]
**bic** | **string** | BIC of the other party bank | [optional]
**is_atm_withdrawal** | **bool** |  | [optional]
**merchant_amount** | [**\KlarnaXsAApi\Model\XsATransactionAmount**](XsATransactionAmount.md) |  | [optional]
**merchant_name** | **string** | Name of the merchant - only for type CARD_TRANSACTION | [optional]
**merchant_country** | **string** | Country of the merchant - only for type CARD_TRANSACTION | [optional]
**merchant_city** | **string** | City of the merchant - only for type CARD_TRANSACTION | [optional]
**merchant_category_code** | **string** | Category code of the payment - only for type CARD_TRANSACTION | [optional]
**foreign_exchange_rate** | **float** | In case the merchant amount is different from the transaction amount the rate will be !&#x3D; 1 - only for type CARD_TRANSACTION | [optional]
**sepa_transaction_type** | **string** | SEPA type of the transaction | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
