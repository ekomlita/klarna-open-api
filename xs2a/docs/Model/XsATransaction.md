# # XsATransaction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID of the transaction |
**amount** | [**\KlarnaXsAApi\Model\XsATransactionAmount**](XsATransactionAmount.md) |  |
**balance_after_transaction** | [**\KlarnaXsAApi\Model\XsATransactionAmount**](XsATransactionAmount.md) |  |
**created_at** | **string** | Created at date of the transaction |
**status** | **string** | Status of the transaction |
**transaction_details** | [**\KlarnaXsAApi\Model\XsATransactionDetails**](XsATransactionDetails.md) |  |
**other_party** | **string** | Other party&#39;s name or information | [optional]
**reference_text** | **string** | Reference of the transaction | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
