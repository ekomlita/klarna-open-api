# # XsAPostFundsConfirmationRequestBody

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** | Fund amount in the smallest unit of given currency (e.g. 1.89 EUR would be { amount: 189 }). |
**currency** | **string** | Currency. |
**email** | **string** | Email address the user used for their Klarna Bank Account. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
