# # XsAPostPaymentRequestBodyStandingOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **string** | Format:yyyy-MM-dd Starting date for standing order payment. |
**end_date** | **string** | Format:yyyy-MM-dd Last date for standing order payment. | [optional]
**frequency** | **string** | Frequency of standing order payment. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
