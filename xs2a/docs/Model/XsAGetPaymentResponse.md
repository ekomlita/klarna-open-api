# # XsAGetPaymentResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID of the payment. | [optional]
**status** | **string** | Status of the payment. | [optional]
**sender_details** | [**\KlarnaXsAApi\Model\XsAGetPaymentResponseSenderDetails**](XsAGetPaymentResponseSenderDetails.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
