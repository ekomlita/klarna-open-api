# OpenAPIClient-php

Our Access to Account (XS2A) API is the PSD2 compliant interface for accessing account information and initiating payments on behalf of Klarna payment account users.

If you are looking for the documentation of the **Open banking. by Klarna** solution, you can find the docs [`here`](https://docs.openbanking.klarna.com/index.html).

You can find more information about PSD2 [`here`](https://www.klarna.com/pay-now/customer-service/what-is-psd2/).


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/ekomlita/klarna-open-api.git"
    }
  ],
  "require": {
    "ekomlita/klarna-open-api": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');




$apiInstance = new KlarnaXsAApi\Api\AccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$consent_id = 'consent_id_example'; // string | The consent-id that was received in the GET /consent-sessions/{consent_session_id} request
$account_id = 'account_id_example'; // string | account id
$x_request_id = 'x_request_id_example'; // string | ID of the request, unique to the call, as determined by the initiating party. You can communicate this header to us to debug any issues with your requests.

try {
    $result = $apiInstance->accountBalanceFlow($consent_id, $account_id, $x_request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->accountBalanceFlow: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.klarna.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountsApi* | [**accountBalanceFlow**](docs/Api/AccountsApi.md#accountbalanceflow) | **GET** /v2/accounts/{account_id}/balance | Get account balance.
*AccountsApi* | [**accountFlow**](docs/Api/AccountsApi.md#accountflow) | **GET** /v2/accounts/{account_id} | Get account details.
*AccountsApi* | [**accountTransactionsFlow**](docs/Api/AccountsApi.md#accounttransactionsflow) | **GET** /v2/accounts/{account_id}/transactions | Get paginated transaction list.
*AccountsApi* | [**accountsFlow**](docs/Api/AccountsApi.md#accountsflow) | **GET** /v2/accounts | Get account list.
*AccountsApi* | [**fundFlow**](docs/Api/AccountsApi.md#fundflow) | **POST** /v2/funds-confirmations | Confirm available funds.
*ConsentsApi* | [**consentFlow**](docs/Api/ConsentsApi.md#consentflow) | **POST** /v2/consent-sessions | Create consent session.
*ConsentsApi* | [**getConsentFlow**](docs/Api/ConsentsApi.md#getconsentflow) | **GET** /v2/consent-sessions/{consent_session_id} | Get consent session.
*PaymentsApi* | [**deletePaymentFlow**](docs/Api/PaymentsApi.md#deletepaymentflow) | **DELETE** /v2/payments/{payment_id} | Revoke a payment
*PaymentsApi* | [**getPaymentFlow**](docs/Api/PaymentsApi.md#getpaymentflow) | **GET** /v2/payments/{payment_id} | Get payment details.
*PaymentsApi* | [**paymentFlow**](docs/Api/PaymentsApi.md#paymentflow) | **POST** /v2/payments | Create payment.

## Models

- [XsAAccount](docs/Model/XsAAccount.md)
- [XsAAccounts](docs/Model/XsAAccounts.md)
- [XsABalance](docs/Model/XsABalance.md)
- [XsAFundsConfirmation](docs/Model/XsAFundsConfirmation.md)
- [XsAGetConsentSessionResponse](docs/Model/XsAGetConsentSessionResponse.md)
- [XsAGetPaymentResponse](docs/Model/XsAGetPaymentResponse.md)
- [XsAGetPaymentResponseSenderDetails](docs/Model/XsAGetPaymentResponseSenderDetails.md)
- [XsAPageInfo](docs/Model/XsAPageInfo.md)
- [XsAPostConsentSessionRequestBody](docs/Model/XsAPostConsentSessionRequestBody.md)
- [XsAPostConsentSessionResponse](docs/Model/XsAPostConsentSessionResponse.md)
- [XsAPostFundsConfirmationRequestBody](docs/Model/XsAPostFundsConfirmationRequestBody.md)
- [XsAPostPaymentRequestBody](docs/Model/XsAPostPaymentRequestBody.md)
- [XsAPostPaymentRequestBodyStandingOrder](docs/Model/XsAPostPaymentRequestBodyStandingOrder.md)
- [XsAPostPaymentRequestBodyTo](docs/Model/XsAPostPaymentRequestBodyTo.md)
- [XsAPostPaymentResponse](docs/Model/XsAPostPaymentResponse.md)
- [XsATransaction](docs/Model/XsATransaction.md)
- [XsATransactionAmount](docs/Model/XsATransactionAmount.md)
- [XsATransactionDetails](docs/Model/XsATransactionDetails.md)
- [XsATransactionsResponse](docs/Model/XsATransactionsResponse.md)

## Authorization
All endpoints do not require authorization.
## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
