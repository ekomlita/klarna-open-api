<?php
/**
 * InstantShoppingCustomerV1
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  KlarnaInstantShoppingApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Instant Shopping
 *
 * The Instant Shopping API is serving two purposes:  1. to manage the orders as they result from the Instant Shopping purchase flow  1. to generate Instant Shopping Button keys necessary for setting up the Instant Shopping flow both onsite and offsite  Note that as soon as a purchase initiated through Instant Shopping is completed within Klarna, the order should be read and handled using the [Order Management API](/order-management/api/).
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace KlarnaInstantShoppingApi\Model;

use \ArrayAccess;
use \KlarnaInstantShoppingApi\ObjectSerializer;

/**
 * InstantShoppingCustomerV1 Class Doc Comment
 *
 * @category Class
 * @package  KlarnaInstantShoppingApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class InstantShoppingCustomerV1 implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'CustomerV1';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'date_of_birth' => 'string',
        'title' => 'string',
        'gender' => 'string',
        'last_four_ssn' => 'string',
        'national_identification_number' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'date_of_birth' => null,
        'title' => null,
        'gender' => null,
        'last_four_ssn' => null,
        'national_identification_number' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'date_of_birth' => 'date_of_birth',
        'title' => 'title',
        'gender' => 'gender',
        'last_four_ssn' => 'last_four_ssn',
        'national_identification_number' => 'national_identification_number'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'date_of_birth' => 'setDateOfBirth',
        'title' => 'setTitle',
        'gender' => 'setGender',
        'last_four_ssn' => 'setLastFourSsn',
        'national_identification_number' => 'setNationalIdentificationNumber'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'date_of_birth' => 'getDateOfBirth',
        'title' => 'getTitle',
        'gender' => 'getGender',
        'last_four_ssn' => 'getLastFourSsn',
        'national_identification_number' => 'getNationalIdentificationNumber'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['date_of_birth'] = $data['date_of_birth'] ?? null;
        $this->container['title'] = $data['title'] ?? null;
        $this->container['gender'] = $data['gender'] ?? null;
        $this->container['last_four_ssn'] = $data['last_four_ssn'] ?? null;
        $this->container['national_identification_number'] = $data['national_identification_number'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if (!is_null($this->container['last_four_ssn']) && !preg_match("/^([0-9]{4}|[0-9]{9})$/", $this->container['last_four_ssn'])) {
            $invalidProperties[] = "invalid value for 'last_four_ssn', must be conform to the pattern /^([0-9]{4}|[0-9]{9})$/.";
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets date_of_birth
     *
     * @return string|null
     */
    public function getDateOfBirth()
    {
        return $this->container['date_of_birth'];
    }

    /**
     * Sets date_of_birth
     *
     * @param string|null $date_of_birth ISO 8601 date. The customer date of birth.
     *
     * @return self
     */
    public function setDateOfBirth($date_of_birth)
    {
        $this->container['date_of_birth'] = $date_of_birth;

        return $this;
    }

    /**
     * Gets title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->container['title'];
    }

    /**
     * Sets title
     *
     * @param string|null $title The customer's title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->container['title'] = $title;

        return $this;
    }

    /**
     * Gets gender
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->container['gender'];
    }

    /**
     * Sets gender
     *
     * @param string|null $gender The customer gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->container['gender'] = $gender;

        return $this;
    }

    /**
     * Gets last_four_ssn
     *
     * @return string|null
     */
    public function getLastFourSsn()
    {
        return $this->container['last_four_ssn'];
    }

    /**
     * Sets last_four_ssn
     *
     * @param string|null $last_four_ssn Last four digits for customer social security number
     *
     * @return self
     */
    public function setLastFourSsn($last_four_ssn)
    {

        if (!is_null($last_four_ssn) && (!preg_match("/^([0-9]{4}|[0-9]{9})$/", $last_four_ssn))) {
            throw new \InvalidArgumentException("invalid value for $last_four_ssn when calling InstantShoppingCustomerV1., must conform to the pattern /^([0-9]{4}|[0-9]{9})$/.");
        }

        $this->container['last_four_ssn'] = $last_four_ssn;

        return $this;
    }

    /**
     * Gets national_identification_number
     *
     * @return string|null
     */
    public function getNationalIdentificationNumber()
    {
        return $this->container['national_identification_number'];
    }

    /**
     * Sets national_identification_number
     *
     * @param string|null $national_identification_number The customer's national identification number
     *
     * @return self
     */
    public function setNationalIdentificationNumber($national_identification_number)
    {
        $this->container['national_identification_number'] = $national_identification_number;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


