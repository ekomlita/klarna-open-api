<?php
/**
 * InstantShoppingProductIdentifiersV1
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  KlarnaInstantShoppingApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Instant Shopping
 *
 * The Instant Shopping API is serving two purposes:  1. to manage the orders as they result from the Instant Shopping purchase flow  1. to generate Instant Shopping Button keys necessary for setting up the Instant Shopping flow both onsite and offsite  Note that as soon as a purchase initiated through Instant Shopping is completed within Klarna, the order should be read and handled using the [Order Management API](/order-management/api/).
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace KlarnaInstantShoppingApi\Model;

use \ArrayAccess;
use \KlarnaInstantShoppingApi\ObjectSerializer;

/**
 * InstantShoppingProductIdentifiersV1 Class Doc Comment
 *
 * @category Class
 * @package  KlarnaInstantShoppingApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class InstantShoppingProductIdentifiersV1 implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'ProductIdentifiersV1';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'category_path' => 'string',
        'global_trade_item_number' => 'string',
        'manufacturer_part_number' => 'string',
        'brand' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'category_path' => null,
        'global_trade_item_number' => null,
        'manufacturer_part_number' => null,
        'brand' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'category_path' => 'category_path',
        'global_trade_item_number' => 'global_trade_item_number',
        'manufacturer_part_number' => 'manufacturer_part_number',
        'brand' => 'brand'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'category_path' => 'setCategoryPath',
        'global_trade_item_number' => 'setGlobalTradeItemNumber',
        'manufacturer_part_number' => 'setManufacturerPartNumber',
        'brand' => 'setBrand'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'category_path' => 'getCategoryPath',
        'global_trade_item_number' => 'getGlobalTradeItemNumber',
        'manufacturer_part_number' => 'getManufacturerPartNumber',
        'brand' => 'getBrand'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['category_path'] = $data['category_path'] ?? null;
        $this->container['global_trade_item_number'] = $data['global_trade_item_number'] ?? null;
        $this->container['manufacturer_part_number'] = $data['manufacturer_part_number'] ?? null;
        $this->container['brand'] = $data['brand'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets category_path
     *
     * @return string|null
     */
    public function getCategoryPath()
    {
        return $this->container['category_path'];
    }

    /**
     * Sets category_path
     *
     * @param string|null $category_path The product's category path as used in the merchant's webshop. Include the full and most detailed category and separate the segments with ' > '
     *
     * @return self
     */
    public function setCategoryPath($category_path)
    {
        $this->container['category_path'] = $category_path;

        return $this;
    }

    /**
     * Gets global_trade_item_number
     *
     * @return string|null
     */
    public function getGlobalTradeItemNumber()
    {
        return $this->container['global_trade_item_number'];
    }

    /**
     * Sets global_trade_item_number
     *
     * @param string|null $global_trade_item_number The product's Global Trade Item Number (GTIN). Common types of GTIN are EAN, ISBN or UPC. Exclude dashes and spaces, where possible
     *
     * @return self
     */
    public function setGlobalTradeItemNumber($global_trade_item_number)
    {
        $this->container['global_trade_item_number'] = $global_trade_item_number;

        return $this;
    }

    /**
     * Gets manufacturer_part_number
     *
     * @return string|null
     */
    public function getManufacturerPartNumber()
    {
        return $this->container['manufacturer_part_number'];
    }

    /**
     * Sets manufacturer_part_number
     *
     * @param string|null $manufacturer_part_number The product's Manufacturer Part Number (MPN), which - together with the brand - uniquely identifies a product. Only submit MPNs assigned by a manufacturer and use the most specific MPN possible
     *
     * @return self
     */
    public function setManufacturerPartNumber($manufacturer_part_number)
    {
        $this->container['manufacturer_part_number'] = $manufacturer_part_number;

        return $this;
    }

    /**
     * Gets brand
     *
     * @return string|null
     */
    public function getBrand()
    {
        return $this->container['brand'];
    }

    /**
     * Sets brand
     *
     * @param string|null $brand The product's brand name as generally recognized by consumers. If no brand is available for a product, do not supply any value.
     *
     * @return self
     */
    public function setBrand($brand)
    {
        $this->container['brand'] = $brand;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


