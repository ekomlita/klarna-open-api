# # InstantShoppingStylingOptionsV1Theme

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**variation** | **string** | Choose between Klarna themes | [optional] [default to 'klarna']
**tagline** | **string** | Choose the color of the tagline, which appears below the button. | [optional] [default to 'dark']
**type** | **string** | Choose between types that affect the text of the button | [optional] [default to 'express']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
