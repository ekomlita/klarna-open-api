# # InstantShoppingSelectedShippingOptionV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Shipping option identifier |
**name** | **string** | Name of shipping option |
**description** | **string** | Description of shipping option | [optional]
**promo** | **string** | Promotion name. To be used if this shipping option is promotional | [optional]
**price** | **int** | Price in cents including tax |
**tax_amount** | **int** | Amount of tax |
**tax_rate** | **int** | Non-negative. In percent, two implicit decimals. I.e 2500 &#x3D; 25% |
**preselected** | **bool** | If the option should be preselected | [optional]
**shipping_method** | **string** | Type of basic shipping method. Possible values:&lt;ul&gt;&lt;li&gt;PickUpStore&lt;/li&gt;&lt;li&gt;Home&lt;/li&gt;&lt;li&gt;BoxReg&lt;/li&gt;&lt;li&gt;BoxUnreg&lt;/li&gt;&lt;li&gt;PickUpPoint&lt;/li&gt;&lt;li&gt;Own&lt;/li&gt;&lt;li&gt;Postal&lt;/li&gt;&lt;li&gt;DHLPackstation&lt;/li&gt;&lt;li&gt;Other&lt;/li&gt;&lt;/ul&gt; | [optional]
**delivery_details** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingDeliveryDetailsV1**](InstantShoppingShippingDeliveryDetailsV1.md) |  | [optional]
**tms_reference** | **string** | The transport management system reference | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
