# # InstantShoppingLocationAddressV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street_address** | **string** | Street address, first line. | [optional]
**street_address2** | **string** | Street address, second line. | [optional]
**postal_code** | **string** | Postal/post code. | [optional]
**city** | **string** | City. | [optional]
**region** | **string** | State or Region. | [optional]
**country** | **string** | ISO 3166 alpha-2 format | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
