# # InstantShoppingMerchantCheckbox

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifier for the checkbox |
**text** | **string** | Text that will be displayed to the consumer aside the checkbox. (max 255 characters) |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
