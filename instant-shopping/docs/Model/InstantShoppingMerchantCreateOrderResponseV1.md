# # InstantShoppingMerchantCreateOrderResponseV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **string** | The unique order ID. No longer than 255 characters. | [optional] [readonly]
**fraud_status** | **string** | Fraud status for the order. | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
