# # InstantShoppingSelectedShippingOptionV1AllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tms_reference** | **string** | The transport management system reference | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
