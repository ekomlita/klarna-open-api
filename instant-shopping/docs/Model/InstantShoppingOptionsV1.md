# # InstantShoppingOptionsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_separate_shipping_address** | **bool** | If set to true the consumer can enter separate shipping address | [optional] [default to false]
**date_of_birth_mandatory** | **bool** | Makes the date of birth mandatory in the address form | [optional] [default to false]
**phone_mandatory** | **bool** | Makes the phone mandatory in the address form | [optional] [default to false]
**national_identification_number_mandatory** | **bool** | Set to true for purchases that MUST have a national insurance number | [optional] [default to false]
**additional_checkboxes** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantCheckbox[]**](InstantShoppingMerchantCheckbox.md) | Additional merchant defined checkboxes. e.g. for Newsletter opt-in. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
