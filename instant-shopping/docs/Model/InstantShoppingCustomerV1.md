# # InstantShoppingCustomerV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_of_birth** | **string** | ISO 8601 date. The customer date of birth. | [optional]
**title** | **string** | The customer&#39;s title | [optional]
**gender** | **string** | The customer gender | [optional]
**last_four_ssn** | **string** | Last four digits for customer social security number | [optional]
**national_identification_number** | **string** | The customer&#39;s national identification number | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
