# # InstantShoppingShippingDeliveryDetailsV1Location

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifier of the pickup location | [optional]
**name** | **string** | Name of the pickup location | [optional]
**address** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingLocationAddressV1**](InstantShoppingLocationAddressV1.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
