# # InstantShoppingShippingDeliveryDetailsV1CarrierProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** | The carrier product identifier | [optional]
**name** | **string** | The carrier product name | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
