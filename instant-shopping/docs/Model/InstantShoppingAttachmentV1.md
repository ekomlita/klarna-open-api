# # InstantShoppingAttachmentV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **string** | This field should be a _string_ containing the body of the attachment. The body should be an object containing any of the keys and sub objects described below serialised to JSON | [optional]
**content_type** | **string** | The content type of the body property. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
