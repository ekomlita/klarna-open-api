# # InstantShoppingMerchantGetOrderResponseV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | The merchant name (max 255 characters). | [optional]
**purchase_country** | **string** | ISO 3166 alpha-2 purchase country. |
**purchase_currency** | **string** | ISO 4217 purchase currency. |
**locale** | **string** | RFC 1766 customer&#39;s locale. |
**billing_address** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingAddressV1**](InstantShoppingAddressV1.md) |  |
**shipping_address** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingAddressV1**](InstantShoppingAddressV1.md) |  | [optional]
**order_amount** | **int** | Non-negative, minor units. Total amount of the order. Must be sum of the order lines and tax amount |
**order_tax_amount** | **int** | Non-negative, minor units. The total tax amount of the order. |
**order_lines** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingOrderLineV1[]**](InstantShoppingOrderLineV1.md) | The applicable order lines (max 1000) |
**merchant_reference1** | **string** | Used for storing merchant&#39;s internal order number or other reference. If set, will be shown on the confirmation page as \&quot;order number\&quot; (max 255 characters). | [optional]
**merchant_reference2** | **string** | Used for storing merchant&#39;s internal order number or other reference (max 255 characters). | [optional]
**merchant_requested** | **array<string,bool>** | A hashmap with merchant-provided checkbox IDs as keys and booleans determining if a consumer has agreed to each checkbox as values. | [optional]
**merchant_urls** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantUrlsV1**](InstantShoppingMerchantUrlsV1.md) |  | [optional]
**customer** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingCustomerV1**](InstantShoppingCustomerV1.md) |  | [optional]
**merchant_data** | **string** | Pass through field (max 1024 characters). | [optional]
**attachment** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingAttachmentV1**](InstantShoppingAttachmentV1.md) |  | [optional]
**shipping_attributes** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingAttributesV1**](InstantShoppingShippingAttributesV1.md) |  | [optional]
**integrator_url** | **string** |  | [optional]
**selected_shipping_option** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingSelectedShippingOptionV1**](InstantShoppingSelectedShippingOptionV1.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
