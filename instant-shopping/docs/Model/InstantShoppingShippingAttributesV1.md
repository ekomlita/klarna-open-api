# # InstantShoppingShippingAttributesV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weight** | **int** | The product&#39;s weight as used in the merchant&#39;s webshop. Non-negative. Measured in grams. | [optional]
**dimensions** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingAttributesV1Dimensions**](InstantShoppingShippingAttributesV1Dimensions.md) |  | [optional]
**tags** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
