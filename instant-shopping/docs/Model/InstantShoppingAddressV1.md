# # InstantShoppingAddressV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**given_name** | **string** | Given name. | [optional]
**family_name** | **string** | Family name. | [optional]
**email** | **string** | E-mail address. | [optional]
**title** | **string** | Title. | [optional]
**street_address** | **string** | Street address, first line. | [optional]
**street_address2** | **string** | Street address, second line. | [optional]
**postal_code** | **string** | Postal/post code. | [optional]
**city** | **string** | City. | [optional]
**region** | **string** | State or Region. | [optional]
**phone** | **string** | Phone number. | [optional]
**country** | **string** | ISO 3166 alpha-2 format | [optional]
**care_of** | **string** | Text for the care of information. | [optional]
**attention** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
