# # InstantShoppingButtonSetupOptionsV1AllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**button_key** | **string** | Identifier of the button key. Readonly value. Set by the server. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
