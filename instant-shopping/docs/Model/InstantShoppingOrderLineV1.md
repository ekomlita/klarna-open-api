# # InstantShoppingOrderLineV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Descriptive item name. |
**type** | **string** | Order line type. Possible values: physical | [optional]
**reference** | **string** | Article number, SKU or similar. Max length is 64 characters. | [optional]
**quantity** | **int** | Non-negative. The item quantity. |
**quantity_unit** | **string** | Unit used to describe the quantity, e.g. \&quot;kg\&quot;, \&quot;pcs\&quot; If defined has to be 1-8 characters | [optional]
**tax_rate** | **int** | Non-negative. In percent, two implicit decimals. I.e 2500 &#x3D; 25%. |
**total_amount** | **int** | Includes tax and discount. Must match (quantity unit_price) - total_discount_amount within ±quantity. (max value: 100000000) |
**total_discount_amount** | **int** | Non-negative minor units. Includes tax. | [optional]
**total_tax_amount** | **int** | Must be within ±1 of total_amount - total_amount 10000 / (10000 + tax_rate). Negative when type is discount. |
**unit_price** | **int** | Minor units. Includes tax, excludes discount. (max value: 100000000) |
**merchant_data** | **string** | Pass through field. (max 255 characters) | [optional]
**product_url** | **string** | URL to an image that can be later embedded in communications between Klarna and the customer. (max 1024 characters) | [optional]
**image_url** | **string** | URL to an image that can be later embedded in communications between Klarna and the customer. (max 1024 characters) | [optional]
**product_identifiers** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingProductIdentifiersV1**](InstantShoppingProductIdentifiersV1.md) |  | [optional]
**shipping_attributes** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingShippingAttributesV1**](InstantShoppingShippingAttributesV1.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
