# # InstantShoppingOrderMerchantUrlsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notification** | **string** | URL of an endpoint at the merchant side, which will receive notifications on pending orders. (must be https, max 2000 characters) | [optional]
**confirmation** | **string** | URL of a page that the consumers will be redirected to after completing a purchase with Instant Shopping. (max 2000 characters) | [optional]
**push** | **string** | URL of an endpoint at the merchant side, which will receive a ping when an order is completed within Klarna&#39;s order management. (must be https, max 2000 characters) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
