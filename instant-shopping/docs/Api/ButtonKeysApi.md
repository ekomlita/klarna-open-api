# KlarnaInstantShoppingApi\ButtonKeysApi

All URIs are relative to https://api.klarna.com/instantshopping.

Method | HTTP request | Description
------------- | ------------- | -------------
[**buttonsKeyGet()**](ButtonKeysApi.md#buttonsKeyGet) | **GET** /v1/buttons/{button_key} | See the setup options for a specific button key.
[**buttonsPost()**](ButtonKeysApi.md#buttonsPost) | **POST** /v1/buttons | Create a button key based on setup options.
[**buttonsPut()**](ButtonKeysApi.md#buttonsPut) | **PUT** /v1/buttons/{button_key} | Update the setup options for a specific button key.


## `buttonsKeyGet()`

```php
buttonsKeyGet($button_key): \KlarnaInstantShoppingApi\Model\InstantShoppingButtonSetupOptionsV1
```

See the setup options for a specific button key.

See the setup options for a specific button key.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaInstantShoppingApi\Api\ButtonKeysApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$button_key = 'button_key_example'; // string

try {
    $result = $apiInstance->buttonsKeyGet($button_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ButtonKeysApi->buttonsKeyGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **button_key** | **string**|  |

### Return type

[**\KlarnaInstantShoppingApi\Model\InstantShoppingButtonSetupOptionsV1**](../Model/InstantShoppingButtonSetupOptionsV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `buttonsPost()`

```php
buttonsPost($instant_shopping_merchant_request_button_setup_options_v1): \KlarnaInstantShoppingApi\Model\InstantShoppingButtonSetupOptionsV1
```

Create a button key based on setup options.

Create a button key based on setup options.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaInstantShoppingApi\Api\ButtonKeysApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$instant_shopping_merchant_request_button_setup_options_v1 = new \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantRequestButtonSetupOptionsV1(); // \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantRequestButtonSetupOptionsV1 | Setup options

try {
    $result = $apiInstance->buttonsPost($instant_shopping_merchant_request_button_setup_options_v1);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ButtonKeysApi->buttonsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **instant_shopping_merchant_request_button_setup_options_v1** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantRequestButtonSetupOptionsV1**](../Model/InstantShoppingMerchantRequestButtonSetupOptionsV1.md)| Setup options |

### Return type

[**\KlarnaInstantShoppingApi\Model\InstantShoppingButtonSetupOptionsV1**](../Model/InstantShoppingButtonSetupOptionsV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `*/*`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `buttonsPut()`

```php
buttonsPut($button_key, $instant_shopping_merchant_request_button_setup_options_v1): \KlarnaInstantShoppingApi\Model\InstantShoppingButtonSetupOptionsV1
```

Update the setup options for a specific button key.

Update the setup options for a specific button key.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaInstantShoppingApi\Api\ButtonKeysApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$button_key = 'button_key_example'; // string
$instant_shopping_merchant_request_button_setup_options_v1 = new \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantRequestButtonSetupOptionsV1(); // \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantRequestButtonSetupOptionsV1 | Setup options

try {
    $result = $apiInstance->buttonsPut($button_key, $instant_shopping_merchant_request_button_setup_options_v1);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ButtonKeysApi->buttonsPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **button_key** | **string**|  |
 **instant_shopping_merchant_request_button_setup_options_v1** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantRequestButtonSetupOptionsV1**](../Model/InstantShoppingMerchantRequestButtonSetupOptionsV1.md)| Setup options |

### Return type

[**\KlarnaInstantShoppingApi\Model\InstantShoppingButtonSetupOptionsV1**](../Model/InstantShoppingButtonSetupOptionsV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
