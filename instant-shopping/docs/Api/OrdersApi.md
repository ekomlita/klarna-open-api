# KlarnaInstantShoppingApi\OrdersApi

All URIs are relative to https://api.klarna.com/instantshopping.

Method | HTTP request | Description
------------- | ------------- | -------------
[**approveAndPlaceKbbOrder()**](OrdersApi.md#approveAndPlaceKbbOrder) | **POST** /v1/authorizations/{authorization_token}/orders | Approve the authorized order and place an order identified by the authorization token
[**declineKbbOrder()**](OrdersApi.md#declineKbbOrder) | **DELETE** /v1/authorizations/{authorization_token} | Decline an authorized order identified by the authorization token
[**getKbbMerchantOrder()**](OrdersApi.md#getKbbMerchantOrder) | **GET** /v1/authorizations/{authorization_token} | Retrieve an authorized order based on the authorization token


## `approveAndPlaceKbbOrder()`

```php
approveAndPlaceKbbOrder($authorization_token, $instant_shopping_merchant_create_order_request_v1): \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantCreateOrderResponseV1
```

Approve the authorized order and place an order identified by the authorization token

Approve the authorized order and place an order identified by the authorization token

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaInstantShoppingApi\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$authorization_token = 'authorization_token_example'; // string
$instant_shopping_merchant_create_order_request_v1 = new \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantCreateOrderRequestV1(); // \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantCreateOrderRequestV1

try {
    $result = $apiInstance->approveAndPlaceKbbOrder($authorization_token, $instant_shopping_merchant_create_order_request_v1);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->approveAndPlaceKbbOrder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization_token** | **string**|  |
 **instant_shopping_merchant_create_order_request_v1** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantCreateOrderRequestV1**](../Model/InstantShoppingMerchantCreateOrderRequestV1.md)|  | [optional]

### Return type

[**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantCreateOrderResponseV1**](../Model/InstantShoppingMerchantCreateOrderResponseV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `declineKbbOrder()`

```php
declineKbbOrder($authorization_token, $instant_shopping_merchant_decline_order_request_v1)
```

Decline an authorized order identified by the authorization token

Decline an authorized order identified by the authorization token

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaInstantShoppingApi\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$authorization_token = 'authorization_token_example'; // string
$instant_shopping_merchant_decline_order_request_v1 = new \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantDeclineOrderRequestV1(); // \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantDeclineOrderRequestV1

try {
    $apiInstance->declineKbbOrder($authorization_token, $instant_shopping_merchant_decline_order_request_v1);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->declineKbbOrder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization_token** | **string**|  |
 **instant_shopping_merchant_decline_order_request_v1** | [**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantDeclineOrderRequestV1**](../Model/InstantShoppingMerchantDeclineOrderRequestV1.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getKbbMerchantOrder()`

```php
getKbbMerchantOrder($authorization_token): \KlarnaInstantShoppingApi\Model\InstantShoppingMerchantGetOrderResponseV1
```

Retrieve an authorized order based on the authorization token

Retrieve an authorized order based on the authorization token

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaInstantShoppingApi\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$authorization_token = 'authorization_token_example'; // string

try {
    $result = $apiInstance->getKbbMerchantOrder($authorization_token);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->getKbbMerchantOrder: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization_token** | **string**|  |

### Return type

[**\KlarnaInstantShoppingApi\Model\InstantShoppingMerchantGetOrderResponseV1**](../Model/InstantShoppingMerchantGetOrderResponseV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
