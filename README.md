# Klarna api

[CheckoutMerchant Api Docs](checkout-merchant/README.md)

[Checkout Api Docs](checkout/README.md)

[CustomerToken Api Docs](customer-token/README.md)

[DisputesApi Api Docs](disputes-api/README.md)

[HppMerchant Api Docs](hpp-merchant/README.md)

[InstantShopping Api Docs](instant-shopping/README.md)

[MerchantCardService Api Docs](merchant-card-service/README.md)

[OrderManagement Api Docs](order-management/README.md)

[Payments Api Docs](payments/README.md)

[Settlements Api Docs](settlements/README.md)

[XsA Api Docs](xs2a/README.md)


## You can regenerate apis by renewing spec files and launching `./generate.sh`

