<?php
/**
 * CheckoutMerchantProductIdentifiersTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaCheckoutMerchantApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Checkout API V3
 *
 * API spec
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace KlarnaCheckoutMerchantApi\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * CheckoutMerchantProductIdentifiersTest Class Doc Comment
 *
 * @category    Class
 * @description CheckoutMerchantProductIdentifiers
 * @package     KlarnaCheckoutMerchantApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class CheckoutMerchantProductIdentifiersTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "CheckoutMerchantProductIdentifiers"
     */
    public function testCheckoutMerchantProductIdentifiers()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "brand"
     */
    public function testPropertyBrand()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "category_path"
     */
    public function testPropertyCategoryPath()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "global_trade_item_number"
     */
    public function testPropertyGlobalTradeItemNumber()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "manufacturer_part_number"
     */
    public function testPropertyManufacturerPartNumber()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
