<?php
/**
 * CheckoutMerchantCheckboxV2Test
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  KlarnaCheckoutMerchantApi
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Klarna Checkout API V3
 *
 * API spec
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace KlarnaCheckoutMerchantApi\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * CheckoutMerchantCheckboxV2Test Class Doc Comment
 *
 * @category    Class
 * @description CheckoutMerchantCheckboxV2
 * @package     KlarnaCheckoutMerchantApi
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class CheckoutMerchantCheckboxV2Test extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "CheckoutMerchantCheckboxV2"
     */
    public function testCheckoutMerchantCheckboxV2()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "text"
     */
    public function testPropertyText()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "checked"
     */
    public function testPropertyChecked()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "required"
     */
    public function testPropertyRequired()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
