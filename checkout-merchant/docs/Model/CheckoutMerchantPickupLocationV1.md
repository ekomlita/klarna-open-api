# # CheckoutMerchantPickupLocationV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Id | [optional]
**name** | **string** | Name of the location | [optional]
**address** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddress**](CheckoutMerchantAddress.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
