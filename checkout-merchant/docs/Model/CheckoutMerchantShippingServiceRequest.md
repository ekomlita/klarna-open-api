# # CheckoutMerchantShippingServiceRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantCustomer**](CheckoutMerchantCustomer.md) |  | [optional]
**locale** | **string** | RFC 1766 customer&#39;s locale. | [optional] [readonly]
**order_amount** | **int** | Non-negative, minor units. Total total amount of the order, including tax and any discounts. |
**order_tax_amount** | **int** | Non-negative, minor units. The total tax amount of the order. |
**order_lines** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrderLine[]**](CheckoutMerchantOrderLine.md) | The applicable order lines (max 1000) |
**billing_address** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1**](CheckoutMerchantAddressV1.md) |  | [optional]
**shipping_address** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAddressV1**](CheckoutMerchantAddressV1.md) |  | [optional]
**selected_shipping_option** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingOption**](CheckoutMerchantShippingOption.md) |  | [optional]
**purchase_currency** | **string** | ISO 4217 purchase currency. |
**merchant_data** | **string** | Pass through field (max 6000 characters). | [optional] [readonly]
**merchant_reference1** | **string** | Used for storing merchant&#39;s internal order number or other reference. If set, will be shown on the confirmation page as \&quot;order number\&quot; (max 255 characters). | [optional] [readonly]
**merchant_reference2** | **string** | Used for storing merchant&#39;s internal order number or other reference (max 255 characters). | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
