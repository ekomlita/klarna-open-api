# # CheckoutMerchantDeliveryDetailsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **string** | Carrier product name | [optional]
**class** | **string** | Type of shipping class | [optional]
**product** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantProductV1**](CheckoutMerchantProductV1.md) |  | [optional]
**timeslot** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantTimeslotV1**](CheckoutMerchantTimeslotV1.md) |  | [optional]
**pickup_location** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantPickupLocationV1**](CheckoutMerchantPickupLocationV1.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
