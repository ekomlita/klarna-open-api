# # CheckoutMerchantShippingServiceResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachment** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantAttachment**](CheckoutMerchantAttachment.md) |  | [optional]
**locale** | **string** | RFC 1766 customer&#39;s locale. | [optional] [readonly]
**order_amount** | **int** | Non-negative, minor units. Total total amount of the order, including tax and any discounts. |
**order_tax_amount** | **int** | Non-negative, minor units. The total tax amount of the order. |
**merchant_data** | **string** | Pass through field (max 1024 characters). | [optional]
**order_lines** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantOrderLine[]**](CheckoutMerchantOrderLine.md) | The applicable order lines (max 1000) |
**shipping_options** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantShippingOption[]**](CheckoutMerchantShippingOption.md) | A list of shipping options available for this order. | [optional]
**purchase_currency** | **string** | ISO 4217 purchase currency. |
**external_payment_methods** | [**\KlarnaCheckoutMerchantApi\Model\CheckoutMerchantPaymentProvider[]**](CheckoutMerchantPaymentProvider.md) | List of external payment methods that will be displayed as part of payment methods in the checkout. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
