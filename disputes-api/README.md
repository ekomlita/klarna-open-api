# OpenAPIClient-php

The Disputes API offers Klarna partners and merchants an easy way to handle their customer Disputes.

Utilizing the available endpoints, you are able to:

- Retrieve a list of Disputes, segregated by a variety of filter parameters.

- Fetch a fully detailed version of a Dispute, including all the associated requests and responses.

- Respond to a Dispute request.

- Download/upload a file attachment linked to a specific request response.

- Accept the dispute loss.

Klarna merchants consuming Disputes API are authenticated via the OAuth 2.0 protocol. As part of your credentials you have the possibility to specify a webhook endpoint to receive notifications when your input is needed for a dispute.


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/ekomlita/klarna-open-api.git"
    }
  ],
  "require": {
    "ekomlita/klarna-open-api": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure API key authorization: apiUser
$config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = KlarnaDisputesApiApi\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new KlarnaDisputesApiApi\Api\DisputesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dispute_id = 'dispute_id_example'; // string | ID of dispute to accept loss for

try {
    $apiInstance->acceptLoss($dispute_id);
} catch (Exception $e) {
    echo 'Exception when calling DisputesApi->acceptLoss: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DisputesApi* | [**acceptLoss**](docs/Api/DisputesApi.md#acceptloss) | **POST** /v1/disputes/{dispute_id}/accept-loss | Accept dispute loss
*DisputesApi* | [**downloadRequestAttachment**](docs/Api/DisputesApi.md#downloadrequestattachment) | **GET** /v1/disputes/{dispute_id}/requests/{request_id}/attachments/{attachment_id} | Download request attachment
*DisputesApi* | [**downloadResponseAttachment**](docs/Api/DisputesApi.md#downloadresponseattachment) | **GET** /v1/disputes/{dispute_id}/requests/{request_id}/responses/{response_id}/attachments/{attachment_id} | Download response attachment
*DisputesApi* | [**getDisputeDetails**](docs/Api/DisputesApi.md#getdisputedetails) | **GET** /v1/disputes/{dispute_id} | Get Dispute Details
*DisputesApi* | [**listDisputes**](docs/Api/DisputesApi.md#listdisputes) | **GET** /v1/disputes | List Disputes
*DisputesApi* | [**respondToDisputeRequest**](docs/Api/DisputesApi.md#respondtodisputerequest) | **POST** /v1/disputes/{dispute_id}/requests/{request_id}/responses | Respond to Dispute Request
*DisputesApi* | [**uploadAttachment**](docs/Api/DisputesApi.md#uploadattachment) | **POST** /v1/disputes/{dispute_id}/attachments | Upload attachment

## Models

- [DisputesApiAttachment](docs/Model/DisputesApiAttachment.md)
- [DisputesApiCustomer](docs/Model/DisputesApiCustomer.md)
- [DisputesApiDisputeDetails](docs/Model/DisputesApiDisputeDetails.md)
- [DisputesApiDisputedOrderLine](docs/Model/DisputesApiDisputedOrderLine.md)
- [DisputesApiDisputesList](docs/Model/DisputesApiDisputesList.md)
- [DisputesApiError](docs/Model/DisputesApiError.md)
- [DisputesApiMerchant](docs/Model/DisputesApiMerchant.md)
- [DisputesApiNewResponse](docs/Model/DisputesApiNewResponse.md)
- [DisputesApiNewResponseRequestedFields](docs/Model/DisputesApiNewResponseRequestedFields.md)
- [DisputesApiOrder](docs/Model/DisputesApiOrder.md)
- [DisputesApiPagination](docs/Model/DisputesApiPagination.md)
- [DisputesApiRequest](docs/Model/DisputesApiRequest.md)
- [DisputesApiResponse](docs/Model/DisputesApiResponse.md)
- [DisputesApiResponseRequestedFields](docs/Model/DisputesApiResponseRequestedFields.md)

## Authorization

### apiUser

- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header


## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
