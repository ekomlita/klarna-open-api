# # DisputesApiNewResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachments** | **float[]** | Attachment IDs of already uploaded attachments to link to the response | [optional]
**comment** | **string** | Merchant comment regarding the associated response | [optional]
**requested_fields** | [**\KlarnaDisputesApiApi\Model\DisputesApiNewResponseRequestedFields**](DisputesApiNewResponseRequestedFields.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
