# # DisputesApiOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **\DateTime** | Creation date of the associated order |
**merchant_data** | **string** | Arbitrary merchant data | [optional]
**merchant_reference1** | **string** | Arbitrary reference used for storing merchant&#39;s internal order number or other reference | [optional]
**merchant_reference2** | **string** | Arbitrary reference used for storing merchant&#39;s internal order number or other reference | [optional]
**order_amount** | **int** | Total amount of the order including tax and any available discounts. The value should be in non-negative minor units |
**order_id** | **string** | Unique identifier of the order associated with the current dispute |
**purchase_currency** | **string** | ISO 4217 purchase currency |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
