# # DisputesApiResponseRequestedFields

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_has_been_contacted** | **string** |  | [optional]
**delivered_with_proof** | **string** |  | [optional]
**delivery_address** | **string** |  | [optional]
**goods_returned** | **string** |  | [optional]
**last_digits_of_phone_for_delivery_notification** | **string** |  | [optional]
**order_already_shipped** | **string** |  | [optional]
**order_is_cancellable** | **string** |  | [optional]
**is_travel_order** | **string** |  | [optional]
**order_already_used** | **string** |  | [optional]
**order_is_refundable** | **string** |  | [optional]
**order_type** | **string** |  | [optional]
**received_payment** | **string** |  | [optional]
**return_info** | **string** |  | [optional]
**service_already_used** | **string** |  | [optional]
**service_delivered_by** | **string** |  | [optional]
**shipment_country** | **string** |  | [optional]
**shipping_carrier** | **string** |  | [optional]
**shipping_date** | **\DateTime** |  | [optional]
**tracking_id** | **string** |  | [optional]
**proof_of_delivery** | [**\KlarnaDisputesApiApi\Model\DisputesApiAttachment[]**](DisputesApiAttachment.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
