# # DisputesApiDisputeDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capture_id** | **string** | Unique identifier for the capture this dispute belongs to | [optional]
**disputed_order_lines** | [**\KlarnaDisputesApiApi\Model\DisputesApiDisputedOrderLine[]**](DisputesApiDisputedOrderLine.md) |  | [optional]
**closed_at** | **\DateTime** | Closing date of the current dispute | [optional]
**closing_reason** | **string** | Reason for closing the dispute | [optional]
**closing_reason_detailed** | **string** | Detailed reason for closing the dispute | [optional]
**customer** | [**\KlarnaDisputesApiApi\Model\DisputesApiCustomer**](DisputesApiCustomer.md) |  | [optional]
**deadline_expires_at** | **\DateTime** | Deadline date for a merchant to reply to the associated request of the current dispute | [optional]
**dispute_id** | **string** | Unique identifier for the dispute |
**investigation_status** | **string** | Investigation status of the dispute |
**merchant** | [**\KlarnaDisputesApiApi\Model\DisputesApiMerchant**](DisputesApiMerchant.md) |  | [optional]
**opened_at** | **\DateTime** | Opening date of the current dispute |
**order** | [**\KlarnaDisputesApiApi\Model\DisputesApiOrder**](DisputesApiOrder.md) |  | [optional]
**reason** | **string** | Reason for opening the dispute | [optional]
**requests** | [**\KlarnaDisputesApiApi\Model\DisputesApiRequest[]**](DisputesApiRequest.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
