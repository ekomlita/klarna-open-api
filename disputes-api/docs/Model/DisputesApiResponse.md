# # DisputesApiResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachments** | [**\KlarnaDisputesApiApi\Model\DisputesApiAttachment[]**](DisputesApiAttachment.md) |  |
**comment** | **string** | Merchant comment regarding the associated response | [optional]
**created_at** | **\DateTime** | Creation date of the merchant response |
**requested_fields** | [**\KlarnaDisputesApiApi\Model\DisputesApiResponseRequestedFields**](DisputesApiResponseRequestedFields.md) |  | [optional]
**response_id** | **int** | Incremental identifier of the merchant response |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
