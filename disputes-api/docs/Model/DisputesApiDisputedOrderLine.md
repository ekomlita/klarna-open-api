# # DisputesApiDisputedOrderLine

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disputed_quantity** | **int** | Amount of disputed quantity for the line item with the specified index. |
**reference** | **string** | Reference information for the line item that was provided by the merchant. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
