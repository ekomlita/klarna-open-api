# # DisputesApiError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_code** | **string** | Machine readable description of the error | [optional]
**error_message** | **string** | Human readable English message intended to aid in debugging | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
