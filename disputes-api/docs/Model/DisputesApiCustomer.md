# # DisputesApiCustomer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Email address of the customer | [optional]
**family_name** | **string** | Last name of the customer |
**given_name** | **string** | First name of the customer |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
