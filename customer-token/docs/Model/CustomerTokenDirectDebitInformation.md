# # CustomerTokenDirectDebitInformation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | **string** |  | [optional]
**masked_number** | **string** | Masked bank account number |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
