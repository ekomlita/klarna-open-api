# # CustomerTokenMerchantUrls

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**confirmation** | **string** | URL of merchant confirmation page. (max 2000 characters) |
**push** | **string** | URL that will be requested when an order is completed. Should be different than checkout and confirmation URLs. (max 2000 characters) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
