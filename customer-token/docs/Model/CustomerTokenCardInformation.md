# # CustomerTokenCardInformation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | **string** | Card brand |
**expiry_date** | **string** | Card expiration date |
**masked_number** | **string** | Masked credit card number |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
