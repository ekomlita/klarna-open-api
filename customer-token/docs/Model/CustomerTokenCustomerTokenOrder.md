# # CustomerTokenCustomerTokenOrder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachment** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenAttachment**](CustomerTokenAttachment.md) |  | [optional]
**auto_capture** | **bool** | Allow merchant to trigger auto capturing. | [optional] [default to false]
**merchant_data** | **string** | Pass through field (max 1024 characters). | [optional]
**merchant_reference1** | **string** | Used for storing merchant&#39;s internal order number or other reference. If set, will be shown on the confirmation page as \&quot;order number\&quot; (max 255 characters). | [optional]
**merchant_reference2** | **string** | Used for storing merchant&#39;s internal order number or other reference (max 255 characters). | [optional]
**merchant_urls** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenMerchantUrls**](CustomerTokenMerchantUrls.md) |  | [optional]
**order_amount** | **int** | Non-negative, minor units. Total amount of the order, including tax and any discounts. |
**order_lines** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenOrderLine[]**](CustomerTokenOrderLine.md) | The applicable order lines (max 1000) |
**order_tax_amount** | **int** | Non-negative, minor units. The total tax amount of the order. |
**payment_method_reference** | **string** | Used for slice it purchases to use terms | [optional]
**purchase_currency** | **string** | ISO 4217 purchase currency. |
**shipping_address** | [**\KlarnaCustomerTokenApi\Model\CustomerTokenAddress**](CustomerTokenAddress.md) |  | [optional]
**usage** | **string** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
