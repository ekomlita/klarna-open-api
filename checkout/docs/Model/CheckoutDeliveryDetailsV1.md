# # CheckoutDeliveryDetailsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **string** | Carrier product name | [optional]
**class** | **string** | Type of shipping class | [optional]
**product** | [**\KlarnaCheckoutApi\Model\CheckoutProductV1**](CheckoutProductV1.md) |  | [optional]
**timeslot** | [**\KlarnaCheckoutApi\Model\CheckoutTimeslotV1**](CheckoutTimeslotV1.md) |  | [optional]
**pickup_location** | [**\KlarnaCheckoutApi\Model\CheckoutPickupLocationV1**](CheckoutPickupLocationV1.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
