# # CheckoutPickupLocationV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Id | [optional]
**name** | **string** | Name of the location | [optional]
**address** | [**\KlarnaCheckoutApi\Model\CheckoutAddress**](CheckoutAddress.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
