# KlarnaHppMerchantApi\SessionsApi

All URIs are relative to https://api.klarna.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createHppSession()**](SessionsApi.md#createHppSession) | **POST** /hpp/v1/sessions | Create a new HPP session
[**disableHppSession()**](SessionsApi.md#disableHppSession) | **DELETE** /hpp/v1/sessions/{session_id} | Disable HPP session
[**distributeHppSession()**](SessionsApi.md#distributeHppSession) | **POST** /hpp/v1/sessions/{session_id}/distribution | Distribute link to the HPP Session to the Consumer
[**getSessionById()**](SessionsApi.md#getSessionById) | **GET** /hpp/v1/sessions/{session_id} | Get HPP session


## `createHppSession()`

```php
createHppSession($hpp_merchant_session_creation_request_v1): \KlarnaHppMerchantApi\Model\HppMerchantSessionCreationResponseV1
```

Create a new HPP session

Create a new HPP session

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaHppMerchantApi\Api\SessionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$hpp_merchant_session_creation_request_v1 = new \KlarnaHppMerchantApi\Model\HppMerchantSessionCreationRequestV1(); // \KlarnaHppMerchantApi\Model\HppMerchantSessionCreationRequestV1 | sessionRequest

try {
    $result = $apiInstance->createHppSession($hpp_merchant_session_creation_request_v1);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionsApi->createHppSession: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hpp_merchant_session_creation_request_v1** | [**\KlarnaHppMerchantApi\Model\HppMerchantSessionCreationRequestV1**](../Model/HppMerchantSessionCreationRequestV1.md)| sessionRequest |

### Return type

[**\KlarnaHppMerchantApi\Model\HppMerchantSessionCreationResponseV1**](../Model/HppMerchantSessionCreationResponseV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `disableHppSession()`

```php
disableHppSession($session_id)
```

Disable HPP session

Disable HPP session

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaHppMerchantApi\Api\SessionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$session_id = 'session_id_example'; // string | HPP session id

try {
    $apiInstance->disableHppSession($session_id);
} catch (Exception $e) {
    echo 'Exception when calling SessionsApi->disableHppSession: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **string**| HPP session id |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `distributeHppSession()`

```php
distributeHppSession($session_id, $hpp_merchant_distribution_request_v1)
```

Distribute link to the HPP Session to the Consumer

Distribute link to the HPP Session to the Consumer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaHppMerchantApi\Api\SessionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$session_id = 'session_id_example'; // string | HPP Session id
$hpp_merchant_distribution_request_v1 = new \KlarnaHppMerchantApi\Model\HppMerchantDistributionRequestV1(); // \KlarnaHppMerchantApi\Model\HppMerchantDistributionRequestV1 | Distribution Request parameters

try {
    $apiInstance->distributeHppSession($session_id, $hpp_merchant_distribution_request_v1);
} catch (Exception $e) {
    echo 'Exception when calling SessionsApi->distributeHppSession: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **string**| HPP Session id |
 **hpp_merchant_distribution_request_v1** | [**\KlarnaHppMerchantApi\Model\HppMerchantDistributionRequestV1**](../Model/HppMerchantDistributionRequestV1.md)| Distribution Request parameters |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getSessionById()`

```php
getSessionById($session_id): \KlarnaHppMerchantApi\Model\HppMerchantSessionResponseV1
```

Get HPP session

Get HPP session

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new KlarnaHppMerchantApi\Api\SessionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$session_id = 'session_id_example'; // string | HPP session id

try {
    $result = $apiInstance->getSessionById($session_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionsApi->getSessionById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **string**| HPP session id |

### Return type

[**\KlarnaHppMerchantApi\Model\HppMerchantSessionResponseV1**](../Model/HppMerchantSessionResponseV1.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
