# # HppMerchantSessionResponseV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorization_token** | **string** | Authorization token (only for KP Sessions) | [optional]
**session_id** | **string** | The id of the HPP Session | [optional]
**status** | **string** | Current HPP Session status | [optional]
**updated_at** | **\DateTime** | Latest status update time | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
