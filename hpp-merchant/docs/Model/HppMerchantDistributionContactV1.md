# # HppMerchantDistributionContactV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | Email where to send the email with the HPP link. Only required if distribution method is &#39;email&#39; | [optional]
**phone** | **string** | Phone number where to send the sms with the HPP link. Only required if distribution method is &#39;sms&#39; | [optional]
**phone_country** | **string** | ISO 3166 alpha-2 phone country. Only required if distribution method is &#39;sms&#39; | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
