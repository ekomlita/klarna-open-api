# # HppMerchantSessionCreationRequestV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant_urls** | [**\KlarnaHppMerchantApi\Model\HppMerchantMerchantUrlsV1**](HppMerchantMerchantUrlsV1.md) |  |
**options** | [**\KlarnaHppMerchantApi\Model\HppMerchantOptionsV1**](HppMerchantOptionsV1.md) |  | [optional]
**payment_session_url** | **string** | URL of the KP Session or KCO Order to be hosted by the HPP Session |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
