# # HppMerchantOptionsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**background_images** | [**\KlarnaHppMerchantApi\Model\HppMerchantBackgroundImageV1[]**](HppMerchantBackgroundImageV1.md) | Array of Images to use for the background. Best matching resolution will be used. | [optional]
**logo_url** | **string** | URL of the logo to be displayed | [optional]
**page_title** | **string** | Title for the Payment Page | [optional]
**payment_fallback** | **bool** | Whether or not the Payment Page will show fallback payment methods to the Consumer if the the specified payment methods fail. Ignored field for KCO Orders. | [optional]
**payment_method_categories** | **string[]** | Payment Method Categories to show on the Payment Page. All available categories will be given to the customer if none is specified using payment_method_category or payment_method_categories. Ignored field for KCO Orders. | [optional]
**payment_method_category** | **string** | Payment Method Category to show on the Payment Page. All available categories will be given to the customer if none is specified using payment_method_category or payment_method_categories. Ignored field for KCO Orders. | [optional]
**purchase_type** | **string** | The type of this purchase | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
