# # HppMerchantDistributionRequestV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact_information** | [**\KlarnaHppMerchantApi\Model\HppMerchantDistributionContactV1**](HppMerchantDistributionContactV1.md) |  |
**method** | **string** | Method used for distribution |
**template** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
