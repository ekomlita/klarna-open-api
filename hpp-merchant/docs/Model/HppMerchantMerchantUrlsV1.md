# # HppMerchantMerchantUrlsV1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**back** | **string** | Back URL | [optional]
**cancel** | **string** | Cancel URL | [optional]
**error** | **string** | System error URL | [optional]
**failure** | **string** | Failure URL | [optional]
**privacy_policy** | **string** | Privacy policy URL | [optional]
**status_update** | **string** | Status update URL | [optional]
**success** | **string** | Success URL | [optional]
**terms** | **string** | Terms URL | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
